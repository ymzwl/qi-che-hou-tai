/*
 Navicat Premium Data Transfer

 Source Server         : 119.45.93.149
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 119.45.93.149:3306
 Source Schema         : ry

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 25/05/2021 19:10:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001773DCCFFC878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001773DCCFFC878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001773DCCFFC878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('RuoyiScheduler', 'VM-0-17-ubuntu1612693508855', 1621941039616, 15000);
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('RuoyiScheduler', 'VM-0-9-ubuntu1621921324973', 1621941037100, 15000);

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1621921330000, -1, 5, 'PAUSED', 'CRON', 1621921325000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1621921335000, -1, 5, 'PAUSED', 'CRON', 1621921326000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1621921340000, -1, 5, 'PAUSED', 'CRON', 1621921327000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for qi_che_che_liang
-- ----------------------------
DROP TABLE IF EXISTS `qi_che_che_liang`;
CREATE TABLE `qi_che_che_liang`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NULL DEFAULT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '微信openid',
  `deviceid` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车载设备deviceid',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qi_che_che_liang
-- ----------------------------
INSERT INTO `qi_che_che_liang` VALUES (1, '黑1234', 'admin', 1, '1', '1', '0', 'admin', '2021-01-26 16:25:45', 'admin', '2021-02-02 10:04:17', '超级管理员', NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (2, '黑AVC815', 'common', 2, '2', '1', '0', 'admin', '2021-01-26 16:25:45', 'admin', '2021-04-07 21:53:17', '普通角色', 'o0wKt5ZfkyiPOXdQb4tcqhaPss2M', '7C:9E:BD:05:21:9E');
INSERT INTO `qi_che_che_liang` VALUES (3, '1111', NULL, NULL, '1', '1', '2', 'admin', '2021-02-01 15:57:23', '', NULL, NULL, 'o0wKt5WHIexcRMhc96I72XWxLdNk', '7C:9E:BD:05:21:9E');
INSERT INTO `qi_che_che_liang` VALUES (4, '2222', NULL, NULL, '1', '1', '2', 'admin', '2021-02-01 15:57:34', '', NULL, NULL, NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (5, '333', NULL, NULL, '1', '1', '2', 'admin', '2021-02-01 15:59:29', '', NULL, NULL, NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (6, '黑88888', NULL, NULL, '1', '1', '0', 'admin', '2021-02-01 16:18:00', 'admin', '2021-04-09 14:48:50', NULL, 'o0wKt5WHIexcRMhc96I72XWxLdNk', '7C:9E:BD:05:21:9E');
INSERT INTO `qi_che_che_liang` VALUES (7, '444', NULL, NULL, '1', '1', '2', 'admin', '2021-02-01 16:25:46', '', NULL, NULL, NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (8, '1111111', NULL, NULL, '1', '1', '2', 'admin', '2021-02-02 10:22:08', '', NULL, NULL, NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (9, '黑AS1234', NULL, NULL, '1', '1', '0', 'admin', '2021-04-01 20:48:07', 'admin', '2021-04-01 20:48:15', NULL, NULL, NULL);
INSERT INTO `qi_che_che_liang` VALUES (10, '黑AS32A5', NULL, NULL, '1', '1', '0', 'admin', '2021-04-11 22:00:40', '', NULL, NULL, 'o0wKt5ZfkyiPOXdQb4tcqhaPss2M', '7C:9E:BD:05:21:9E');
INSERT INTO `qi_che_che_liang` VALUES (11, '123456567', NULL, NULL, '1', '1', '0', 'admin', '2021-04-12 15:18:33', 'admin', '2021-05-25 13:51:51', NULL, 'o0wKt5ZfkyiPOXdQb4tcqhaPss2M', '7C:9E:BD:04:88:0E');
INSERT INTO `qi_che_che_liang` VALUES (12, '苏D1234', NULL, NULL, '1', '1', '2', 'admin', '2021-05-22 13:47:59', '', NULL, NULL, 'o0wKt5ZfkyiPOXdQb4tcqhaPss2M', '08:3A:F2:4A:E8:86');
INSERT INTO `qi_che_che_liang` VALUES (13, '苏D12345', NULL, NULL, '1', '1', '0', 'admin', '2021-05-22 14:06:45', '', NULL, NULL, 'o0wKt5ZfkyiPOXdQb4tcqhaPss2M', '08:3A:F2:4A:E8:86');
INSERT INTO `qi_che_che_liang` VALUES (14, '苏D4444', NULL, NULL, '1', '1', '0', 'admin', '2021-05-24 11:25:29', 'admin', '2021-05-24 11:32:01', NULL, 'o0wKt5QjDQ20k_16N9vqe7K_3gG4', '08:3A:F2:4A:E8:86');
INSERT INTO `qi_che_che_liang` VALUES (15, '模式2', NULL, NULL, '1', '1', '0', 'admin', '2021-05-25 13:51:24', '', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for qi_che_che_liang_kong_zhi
-- ----------------------------
DROP TABLE IF EXISTS `qi_che_che_liang_kong_zhi`;
CREATE TABLE `qi_che_che_liang_kong_zhi`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qi_che_che_liang_kong_zhi
-- ----------------------------
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 123);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 124);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 125);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 126);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 127);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 128);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 129);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 130);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (1, 131);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (2, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (6, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (9, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (9, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (9, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (9, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (10, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (11, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (13, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 141);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 144);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 145);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (14, 150);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 100);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 140);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 142);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 143);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 146);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 147);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 148);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 149);
INSERT INTO `qi_che_che_liang_kong_zhi` VALUES (15, 150);

-- ----------------------------
-- Table structure for qi_che_kong_zhi
-- ----------------------------
DROP TABLE IF EXISTS `qi_che_kong_zhi`;
CREATE TABLE `qi_che_kong_zhi`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `tuBiaoLuJing` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '图标路径',
  `tuBiaoDianJiLuJing` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '图标点击路径',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 156 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qi_che_kong_zhi
-- ----------------------------
INSERT INTO `qi_che_kong_zhi` VALUES (100, 0, '0', '汽车控制', 0, '', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:37', 'admin', '2021-05-25 14:49:14', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:37', 'admin', '2021-01-27 13:49:00', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:37', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', 'admin', '2021-01-27 13:49:00', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-01-26 16:25:38', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (110, 103, '0,100,101,103', 'ttt', 1, '', '', '', '0', '2', 'admin', '2021-01-27 11:37:42', 'admin', '2021-01-27 13:49:00', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (111, 103, '0,100,101,103', 'ttt1', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-27 13:49:31', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (112, 111, '0,100,101,103,111', '111', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-27 13:51:35', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (113, 112, '0,100,101,103,111,112', '222', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-27 13:51:46', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (114, 113, '0,100,101,103,111,112,113', '33', 3, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-27 14:20:13', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (115, 100, '0,100', '上氛围灯', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:46:41', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (116, 100, '0,100', '开', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:48:04', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (117, 115, '0,100,115', '开', 1, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:48:19', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (118, 100, '0,100', '上氛围灯', 2, '', '', '', '0', '2', 'admin', '2021-01-28 08:55:34', 'admin', '2021-01-28 09:01:37', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (119, 123, '0,100,123', '开', 1, '', '', '', '0', '2', 'admin', '2021-01-28 08:55:56', 'admin', '2021-01-28 09:03:42', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (120, 118, '0,100,118', '关', 2, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:56:02', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (121, 118, '0,100,118', '红色', 3, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:56:20', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (122, 118, '0,100,118', '亮度加', 4, NULL, NULL, NULL, '0', '2', 'admin', '2021-01-28 08:56:37', '', NULL, NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (123, 100, '0,100', '灯光', 1, '', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:01:11', 'admin', '2021-02-07 18:17:29', '/profile//2021/02/06/617ed63c-3003-44bd-babc-16c76bfb1523.png', '/profile//2021/02/06/e49fff0a-69a1-4fff-a394-81043b3465fc.png');
INSERT INTO `qi_che_kong_zhi` VALUES (124, 123, '0,100,123', '上氛围灯', 1, '', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:02:17', 'admin', '2021-02-07 18:17:29', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (125, 124, '0,100,123,124', '开', 1, '001', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:04:40', 'admin', '2021-02-07 18:17:24', '/profile//2021/02/01/374bd292-5d7e-4f95-a70d-55e7f5a1c688.jpg', '/profile//2021/02/01/5520dcad-25b8-44ea-b3c5-70f4088a4010.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (126, 124, '0,100,123,124', '关', 2, '002', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:04:48', 'admin', '2021-02-07 18:17:26', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (127, 124, '0,100,123,124', '红色', 3, '003', '', '', '0', '2', 'admin', '2021-01-28 09:04:55', 'admin', '2021-02-07 18:17:27', '/profile//2021/02/01/657b5152-0ffe-4fa8-8569-cbbfc637ebfe.jpg', '/profile//2021/02/01/47c7c54f-d26b-4ac2-b04f-fbad7c6a89c4.jpg');
INSERT INTO `qi_che_kong_zhi` VALUES (128, 124, '0,100,123,124', '亮度+', 4, '004', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:05:16', 'admin', '2021-02-07 18:17:29', NULL, NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (129, 100, '0,100', '座椅', 2, '', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:05:46', 'admin', '2021-02-07 18:17:33', '/profile//2021/02/06/09dac06c-cbbb-4d3d-9ec6-340c947cffbd.jpg', '/profile//2021/02/06/c1d5720c-a931-456d-aa5e-083dbe2693cf.jpg');
INSERT INTO `qi_che_kong_zhi` VALUES (130, 129, '0,100,129', '向前', 1, '006', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:05:57', 'admin', '2021-02-07 18:17:32', '/profile//2021/02/01/d818a72d-2759-44df-b632-b08d5d22945c.jpg', '/profile//2021/02/01/9e976c6d-bb1e-4336-a1a6-c1ef5e409dc9.jpg');
INSERT INTO `qi_che_kong_zhi` VALUES (131, 129, '0,100,129', '向后', 2, '007', NULL, NULL, '0', '2', 'admin', '2021-01-28 09:06:04', 'admin', '2021-02-01 13:45:30', '/profile//2021/02/01/f644b898-68cf-43b8-bd01-b08f6c9efedc.jpg', '/profile//2021/02/01/0f1fb52a-e8a4-4096-8820-a04cddb3f815.jpg');
INSERT INTO `qi_che_kong_zhi` VALUES (132, 100, '0,100', '1', 1, '1', NULL, NULL, '0', '2', 'admin', '2021-01-29 15:02:28', '', NULL, '/profile//2021/01/29/5cb52a21-7085-4f31-91bf-264786f0d1f7.jpeg', '/profile//2021/01/29/e34c1671-d8ed-463f-9acf-f59e88de958f.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (133, 100, '0,100', '2', 2, '2', NULL, NULL, '0', '2', 'admin', '2021-01-29 16:14:47', '', NULL, '/profile//2021/01/29/5cb52a21-7085-4f31-91bf-264786f0d1f7.jpeg', '/profile//2021/01/29/e34c1671-d8ed-463f-9acf-f59e88de958f.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (134, 100, '0,100', '3', 3, '3', NULL, NULL, '0', '2', 'admin', '2021-01-29 16:16:20', '', NULL, '/profile//2021/01/29/5cb52a21-7085-4f31-91bf-264786f0d1f7.jpeg', '/profile//2021/01/29/e34c1671-d8ed-463f-9acf-f59e88de958f.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (135, 100, '0,100', '5', 3, '3', NULL, NULL, '0', '2', 'admin', '2021-01-29 16:40:21', '', NULL, '/profile//2021/01/29/5cb52a21-7085-4f31-91bf-264786f0d1f7.jpeg', '/profile//2021/01/29/e34c1671-d8ed-463f-9acf-f59e88de958f.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (136, 100, '0,100', '4', 1, '1', NULL, NULL, '0', '2', 'admin', '2021-01-29 16:52:14', '', NULL, '/profile//2021/01/29/5cb52a21-7085-4f31-91bf-264786f0d1f7.jpeg', '/profile//2021/01/29/e34c1671-d8ed-463f-9acf-f59e88de958f.jpeg');
INSERT INTO `qi_che_kong_zhi` VALUES (137, 129, '0,100,129', '11111', 1111, '321321321321', NULL, NULL, '0', '2', 'admin', '2021-01-29 16:53:00', 'admin', '2021-02-01 13:18:14', '/profile//2021/02/01/38957ab5-ddc5-4b9c-b71e-9708abab375b.jpg', '/profile//2021/02/01/cb7fef70-e597-43c1-b34a-d4bde1d86b49.jpg');
INSERT INTO `qi_che_kong_zhi` VALUES (138, 129, '0,100,129', '向后', 2, '111', NULL, NULL, '0', '2', 'admin', '2021-02-02 10:29:30', 'admin', '2021-02-07 18:17:33', '/img/mo_ren1.png', '/img/mo_ren2.png');
INSERT INTO `qi_che_kong_zhi` VALUES (139, 100, '0,100', '123', 123, '123', NULL, NULL, '0', '2', 'admin', '2021-02-07 17:44:09', 'admin', '2021-02-07 18:17:35', '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (140, 100, '0,100', '灯光', 1, '', NULL, NULL, '0', '0', 'admin', '2021-02-07 18:18:43', 'admin', '2021-04-01 20:46:15', '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (141, 140, '0,100,140', '上氛围灯', 1, '', NULL, NULL, '0', '0', 'admin', '2021-02-07 18:34:52', 'admin', '2021-04-01 20:46:15', '/profile//2021/04/01/6f6ddbed-c86c-4bb1-993b-6dd51c6fbb89.bmp', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (142, 100, '0,100', '座椅', 2, '', NULL, NULL, '0', '0', 'admin', '2021-02-07 18:35:09', 'admin', '2021-04-01 20:18:18', '/profile//2021/04/01/6be76f4f-38aa-4fdb-b2ca-eab2f93e519d.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (143, 140, '0,100,140', '下氛围灯', 2, '', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:56:59', 'admin', '2021-04-01 20:16:55', '/profile//2021/04/01/e7d526bb-d49c-4910-9dd0-d34a87d00b7d.bmp', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (144, 141, '0,100,140,141', '开', 1, '111', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:57:26', 'admin', '2021-04-01 20:16:28', '/profile//2021/04/01/6189e4ba-f4f4-4cfb-be8c-be85be0e7c17.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (145, 141, '0,100,140,141', '关', 2, '222', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:57:37', 'admin', '2021-04-01 20:46:15', '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (146, 143, '0,100,140,143', '开', 1, '333', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:57:51', 'admin', '2021-04-01 20:16:49', '/profile//2021/04/01/7b12587e-fa16-4a7f-a3c8-1212907525ff.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (147, 143, '0,100,140,143', '关', 2, '444', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:58:03', 'admin', '2021-04-01 20:16:55', '/profile//2021/04/01/6e9caba4-8d3c-4e4a-95a3-357c788225c0.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (148, 142, '0,100,142', '向前', 1, '555', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:58:28', 'admin', '2021-04-01 20:18:13', '/profile//2021/04/01/9989045c-7c07-44ee-b5b1-e42489edd2ac.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (149, 142, '0,100,142', '向后', 2, '666', NULL, NULL, '0', '0', 'admin', '2021-02-07 20:58:47', 'admin', '2021-04-01 20:18:18', '/profile//2021/04/01/76d44974-e4a1-4e30-93f6-eb25c1c8a113.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (150, 100, '0,100', '电视', 3, '555', NULL, NULL, '0', '0', 'admin', '2021-05-24 11:31:50', '', NULL, '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (151, 100, '0,100', '空调', 3, NULL, NULL, NULL, '0', '0', 'admin', '2021-05-25 13:51:39', 'admin', '2021-05-25 14:49:14', '/profile//2021/05/25/0269c6df-7e44-4518-9c96-a3b430acaa3e.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (152, 100, '0,100', '空调1挡', 2, NULL, NULL, NULL, '0', '2', 'admin', '2021-05-25 13:52:02', '', NULL, '/profile//2021/05/25/dfaf56c6-36ba-4248-a2de-b3902f35ca59.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (153, 151, '0,100,151', '空调1挡', 1, '8888', NULL, NULL, '0', '0', 'admin', '2021-05-25 13:52:35', 'admin', '2021-05-25 14:49:00', '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (154, 151, '0,100,151', '空调2挡', 2, '999', NULL, NULL, '0', '0', 'admin', '2021-05-25 13:52:54', 'admin', '2021-05-25 14:49:07', '/img/mo_ren1.png', NULL);
INSERT INTO `qi_che_kong_zhi` VALUES (155, 151, '0,100,151', '空调3挡', 3, 'aaa', NULL, NULL, '0', '0', 'admin', '2021-05-25 13:53:10', 'admin', '2021-05-25 14:49:14', '/img/mo_ren1.png', NULL);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2021-01-26 16:26:16', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES (9, '主框架页-是否开启页脚', 'sys.index.ignoreFooter', 'true', 'Y', 'admin', '2021-01-26 16:26:17', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:37', 'admin', '2021-01-28 09:03:17');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:37', 'admin', '2021-01-28 09:03:17');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:37', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', 'admin', '2021-01-28 09:03:17');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-01-26 16:25:38', '', NULL);
INSERT INTO `sys_dept` VALUES (110, 103, '0,100,101,103', 'ttt', 1, '', '', '', '0', '0', 'admin', '2021-01-27 11:37:42', 'admin', '2021-01-27 13:49:00');
INSERT INTO `sys_dept` VALUES (111, 103, '0,100,101,103', 'ttt1', 1, NULL, NULL, NULL, '0', '0', 'admin', '2021-01-27 13:49:31', '', NULL);
INSERT INTO `sys_dept` VALUES (112, 103, '0,100,101,103', '111', 1, '', '', '', '0', '0', 'admin', '2021-01-27 13:51:35', 'admin', '2021-01-28 09:03:17');
INSERT INTO `sys_dept` VALUES (113, 112, '0,100,101,103,112', '222', 1, NULL, NULL, NULL, '0', '0', 'admin', '2021-01-27 13:51:46', '', NULL);
INSERT INTO `sys_dept` VALUES (114, 113, '0,100,101,103,112,113', '33', 3, NULL, NULL, NULL, '0', '0', 'admin', '2021-01-27 14:20:13', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-01-26 16:26:12', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-01-26 16:26:12', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-01-26 16:26:12', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-01-26 16:26:12', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:12', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-01-26 16:26:13', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-01-26 16:26:14', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-01-26 16:26:10', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-01-26 16:26:21', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-01-26 16:26:21', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-01-26 16:26:21', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '系统默认（有参） 总共耗时：42毫秒', '0', '', '2021-04-06 18:48:30');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '221.212.191.138', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 10:57:42');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '221.212.191.138', 'XX XX', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-01-27 11:41:02');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '221.212.191.138', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 11:41:14');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-27 14:19:57');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 14:20:01');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-01-27 14:48:22');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 14:48:26');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-01-27 14:48:45');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 14:48:50');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 14:51:29');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 16:29:34');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-27 16:36:14');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-27 16:36:18');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-01-27 16:57:46');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-01-27 16:57:51');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-27 16:57:55');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 10:10:17');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2021-01-29 11:52:11');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 11:52:43');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-01-29 13:32:46');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-01-29 13:32:55');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 13:32:59');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 14:47:45');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 14:56:03');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 15:00:50');
INSERT INTO `sys_logininfor` VALUES (26, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-01-29 15:36:32');
INSERT INTO `sys_logininfor` VALUES (27, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 15:36:35');
INSERT INTO `sys_logininfor` VALUES (28, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 15:41:51');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 15:54:05');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 16:11:56');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 16:36:35');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 16:39:41');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-29 16:51:56');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 08:18:42');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 08:22:37');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-02-01 08:53:34');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 08:53:37');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 10:15:27');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 11:40:08');
INSERT INTO `sys_logininfor` VALUES (40, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-02-01 11:43:11');
INSERT INTO `sys_logininfor` VALUES (41, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 11:43:15');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 11:50:53');
INSERT INTO `sys_logininfor` VALUES (43, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 11:59:33');
INSERT INTO `sys_logininfor` VALUES (44, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 12:59:53');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 13:06:28');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 13:14:21');
INSERT INTO `sys_logininfor` VALUES (47, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-02-01 13:17:39');
INSERT INTO `sys_logininfor` VALUES (48, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 13:17:41');
INSERT INTO `sys_logininfor` VALUES (49, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 14:41:35');
INSERT INTO `sys_logininfor` VALUES (50, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 14:47:28');
INSERT INTO `sys_logininfor` VALUES (51, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:00:48');
INSERT INTO `sys_logininfor` VALUES (52, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:03:16');
INSERT INTO `sys_logininfor` VALUES (53, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:10:58');
INSERT INTO `sys_logininfor` VALUES (54, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:14:03');
INSERT INTO `sys_logininfor` VALUES (55, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:21:13');
INSERT INTO `sys_logininfor` VALUES (56, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:27:20');
INSERT INTO `sys_logininfor` VALUES (57, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:55:34');
INSERT INTO `sys_logininfor` VALUES (58, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 15:59:18');
INSERT INTO `sys_logininfor` VALUES (59, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 16:17:47');
INSERT INTO `sys_logininfor` VALUES (60, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 16:25:13');
INSERT INTO `sys_logininfor` VALUES (61, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 16:46:08');
INSERT INTO `sys_logininfor` VALUES (62, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 16:50:39');
INSERT INTO `sys_logininfor` VALUES (63, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-01 17:00:29');
INSERT INTO `sys_logininfor` VALUES (64, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 09:47:41');
INSERT INTO `sys_logininfor` VALUES (65, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 09:49:30');
INSERT INTO `sys_logininfor` VALUES (66, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 09:56:26');
INSERT INTO `sys_logininfor` VALUES (67, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 10:13:26');
INSERT INTO `sys_logininfor` VALUES (68, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2021-02-02 10:34:56');
INSERT INTO `sys_logininfor` VALUES (69, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 10:35:07');
INSERT INTO `sys_logininfor` VALUES (70, 'admin', '1.189.193.93', 'XX XX', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-02-02 10:37:46');
INSERT INTO `sys_logininfor` VALUES (71, 'admin', '1.189.193.93', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-02 10:37:58');
INSERT INTO `sys_logininfor` VALUES (72, 'admin', '1.189.193.93', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-02 10:38:33');
INSERT INTO `sys_logininfor` VALUES (73, 'admin', '1.189.193.93', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-02 11:10:59');
INSERT INTO `sys_logininfor` VALUES (74, 'root', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '1', '用户不存在/密码错误', '2021-02-02 14:17:40');
INSERT INTO `sys_logininfor` VALUES (75, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2021-02-02 14:17:49');
INSERT INTO `sys_logininfor` VALUES (76, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 14:18:13');
INSERT INTO `sys_logininfor` VALUES (77, 'admin', '1.189.164.52', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-02 15:45:07');
INSERT INTO `sys_logininfor` VALUES (78, 'admin', '1.189.164.52', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-02 15:46:17');
INSERT INTO `sys_logininfor` VALUES (79, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 15:50:03');
INSERT INTO `sys_logininfor` VALUES (80, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 16:23:57');
INSERT INTO `sys_logininfor` VALUES (81, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 16:46:42');
INSERT INTO `sys_logininfor` VALUES (82, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-02 16:59:05');
INSERT INTO `sys_logininfor` VALUES (83, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-03 18:38:39');
INSERT INTO `sys_logininfor` VALUES (84, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2021-02-03 18:39:13');
INSERT INTO `sys_logininfor` VALUES (85, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-03 18:40:46');
INSERT INTO `sys_logininfor` VALUES (86, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-03 18:53:07');
INSERT INTO `sys_logininfor` VALUES (87, 'admin', '1.58.195.229', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-05 00:40:08');
INSERT INTO `sys_logininfor` VALUES (88, 'admin', '120.244.14.170', 'XX XX', 'Chrome 8', 'Mac OS X', '0', '登录成功', '2021-02-05 15:00:29');
INSERT INTO `sys_logininfor` VALUES (89, 'admin', '1.58.195.229', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-05 15:43:09');
INSERT INTO `sys_logininfor` VALUES (90, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-06 13:52:52');
INSERT INTO `sys_logininfor` VALUES (91, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-06 14:40:54');
INSERT INTO `sys_logininfor` VALUES (92, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-06 15:16:05');
INSERT INTO `sys_logininfor` VALUES (93, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-02-07 17:33:15');
INSERT INTO `sys_logininfor` VALUES (94, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-07 17:33:28');
INSERT INTO `sys_logininfor` VALUES (95, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2021-02-07 17:37:26');
INSERT INTO `sys_logininfor` VALUES (96, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-07 17:37:31');
INSERT INTO `sys_logininfor` VALUES (97, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-07 17:39:45');
INSERT INTO `sys_logininfor` VALUES (98, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-02-07 17:50:16');
INSERT INTO `sys_logininfor` VALUES (99, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-02-07 17:50:24');
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-07 17:50:26');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-07 17:52:26');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-07 18:25:40');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-07 18:26:47');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '125.211.31.149', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-02-07 20:54:50');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '221.212.191.138', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-22 09:05:37');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '120.244.14.80', 'XX XX', 'Chrome 8', 'Mac OS X', '0', '登录成功', '2021-02-22 16:18:45');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '120.244.14.80', 'XX XX', 'Chrome 8', 'Mac OS X', '0', '登录成功', '2021-02-22 16:19:03');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '221.212.191.138', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-02-22 16:26:19');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '114.249.211.205', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-03-10 12:29:27');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '114.249.211.205', 'XX XX', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-03-10 12:30:20');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '61.157.243.120', 'XX XX', 'Firefox 8', 'Windows 10', '0', '登录成功', '2021-03-10 12:32:50');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '1.58.195.45', 'XX XX', 'Chrome', 'Windows 10', '0', '登录成功', '2021-03-31 20:53:23');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '43.225.208.38', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-01 20:42:45');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '101.246.227.254', 'XX XX', 'Chrome 9', 'Linux', '0', '登录成功', '2021-04-03 20:09:59');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '43.225.208.71', 'XX XX', 'Chrome 9', 'Linux', '0', '登录成功', '2021-04-07 21:45:47');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-09 14:45:37');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-09 16:22:49');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-04-09 16:23:10');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Linux', '0', '登录成功', '2021-04-11 21:59:37');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-12 11:10:32');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-12 11:12:31');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-12 11:13:34');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-04-12 11:30:51');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Firefox 8', 'Windows 10', '0', '登录成功', '2021-04-30 19:12:31');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-03 10:16:16');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-05 13:14:37');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-08 01:03:50');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-05-08 01:04:39');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Firefox 8', 'Windows 10', '1', '验证码错误', '2021-05-13 09:52:03');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Firefox 8', 'Windows 10', '0', '登录成功', '2021-05-15 14:01:53');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-05-16 14:20:05');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-05-19 07:54:27');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '1', '验证码错误', '2021-05-19 07:54:38');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '1', '验证码错误', '2021-05-19 07:54:45');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '1', '验证码错误', '2021-05-19 07:54:51');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '1', '验证码错误', '2021-05-19 07:55:00');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-05-19 07:55:13');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Internet Explorer 11', 'Windows 10', '0', '登录成功', '2021-05-22 13:46:21');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2021-05-23 10:29:08');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-05-23 20:58:56');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-24 11:23:55');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-24 14:55:51');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-24 15:01:06');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-05-25 13:47:59');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-05-25 13:48:10');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2021-05-25 13:57:16');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误2次', '2021-05-25 13:57:22');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误3次', '2021-05-25 13:57:29');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1065 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-01-26 16:25:47', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', 'menuItem', 'M', '1', '1', '', 'fa fa-video-camera', 'admin', '2021-01-26 16:25:47', 'admin', '2021-02-02 10:34:10', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', 'menuItem', 'M', '1', '1', '', 'fa fa-bars', 'admin', '2021-01-26 16:25:47', 'admin', '2021-02-02 10:33:38', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', 'menuBlank', 'C', '1', '1', '', 'fa fa-location-arrow', 'admin', '2021-01-26 16:25:47', 'admin', '2021-02-02 10:33:20', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2021-01-26 16:25:47', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2021-01-26 16:25:47', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2021-01-26 16:25:47', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2021-01-26 16:25:47', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2021-01-26 16:25:48', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2021-01-26 16:25:48', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2021-01-26 16:25:48', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2021-01-26 16:25:48', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-01-26 16:25:48', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2021-01-26 16:25:48', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2021-01-26 16:25:48', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2021-01-26 16:25:48', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2021-01-26 16:25:48', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2021-01-26 16:25:48', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2021-01-26 16:25:48', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2021-01-26 16:25:48', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2021-01-26 16:25:48', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-01-26 16:25:48', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-01-26 16:25:48', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-01-26 16:25:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-01-26 16:25:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-01-26 16:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2021-01-26 16:25:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2021-01-26 16:25:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-01-26 16:25:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1062, '汽车管理', 0, 0, '#', 'menuItem', 'M', '0', '1', '', 'fa fa-cab', 'admin', '2021-01-27 14:45:45', 'admin', '2021-01-27 14:48:42', '');
INSERT INTO `sys_menu` VALUES (1063, '控制管理', 1062, 1, '/qiChe/kongZhi', 'menuItem', 'C', '0', '1', 'qiChe:kongZhi:view', '#', 'admin', '2021-01-27 14:47:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1064, '车辆管理', 1062, 2, '/qiChe/cheLiang', 'menuItem', 'C', '0', '1', 'qiChe:cheLiang:view', '#', 'admin', '2021-01-29 10:11:36', 'admin', '2021-01-29 10:12:02', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2021-01-26 16:26:25', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2021-01-26 16:26:25', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 278 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '部门管理', 1, 'com.ruoyi.project.system.dept.controller.DeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '221.212.191.138', 'XX XX', '{\"parentId\":[\"103\"],\"deptName\":[\"ttt\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 11:37:42');
INSERT INTO `sys_oper_log` VALUES (2, '部门管理', 2, 'com.ruoyi.project.system.dept.controller.DeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"110\"],\"parentId\":[\"103\"],\"parentName\":[\"研发部门\"],\"deptName\":[\"ttt\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 13:48:52');
INSERT INTO `sys_oper_log` VALUES (3, '部门管理', 2, 'com.ruoyi.project.system.dept.controller.DeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"110\"],\"parentId\":[\"103\"],\"parentName\":[\"研发部门\"],\"deptName\":[\"ttt\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 13:49:00');
INSERT INTO `sys_oper_log` VALUES (4, '部门管理', 1, 'com.ruoyi.project.system.dept.controller.DeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"103\"],\"deptName\":[\"ttt1\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 13:49:31');
INSERT INTO `sys_oper_log` VALUES (5, '部门管理', 1, 'com.ruoyi.project.system.dept.controller.DeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '221.212.191.138', 'XX XX', '{\"parentId\":[\"111\"],\"deptName\":[\"111\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 13:51:35');
INSERT INTO `sys_oper_log` VALUES (6, '部门管理', 1, 'com.ruoyi.project.system.dept.controller.DeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '221.212.191.138', 'XX XX', '{\"parentId\":[\"112\"],\"deptName\":[\"222\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 13:51:46');
INSERT INTO `sys_oper_log` VALUES (7, '部门管理', 1, 'com.ruoyi.project.system.dept.controller.DeptController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"113\"],\"deptName\":[\"33\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:20:14');
INSERT INTO `sys_oper_log` VALUES (8, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"汽车管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"fa fa-cab\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:45:46');
INSERT INTO `sys_oper_log` VALUES (9, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1062\"],\"menuType\":[\"C\"],\"menuName\":[\"控制管理\"],\"url\":[\"/qiChe/kongZhi\"],\"target\":[\"menuItem\"],\"perms\":[\"qiChe:kongZhi:view\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:47:34');
INSERT INTO `sys_oper_log` VALUES (10, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1062\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"汽车管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"0\"],\"icon\":[\"fa fa-cab\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:48:43');
INSERT INTO `sys_oper_log` VALUES (11, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/114', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:51:52');
INSERT INTO `sys_oper_log` VALUES (12, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/113', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:53:18');
INSERT INTO `sys_oper_log` VALUES (13, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/112', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:53:29');
INSERT INTO `sys_oper_log` VALUES (14, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/102', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"存在下级部门,不允许删除\",\"code\":301}', 0, NULL, '2021-01-27 14:53:42');
INSERT INTO `sys_oper_log` VALUES (15, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/110', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:53:51');
INSERT INTO `sys_oper_log` VALUES (16, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/111', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:53:53');
INSERT INTO `sys_oper_log` VALUES (17, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/104', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 14:54:02');
INSERT INTO `sys_oper_log` VALUES (18, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/105', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"部门存在用户,不允许删除\",\"code\":301}', 0, NULL, '2021-01-27 14:54:04');
INSERT INTO `sys_oper_log` VALUES (19, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/105', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"部门存在用户,不允许删除\",\"code\":301}', 0, NULL, '2021-01-27 14:54:09');
INSERT INTO `sys_oper_log` VALUES (20, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/103', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"部门存在用户,不允许删除\",\"code\":301}', 0, NULL, '2021-01-27 16:29:41');
INSERT INTO `sys_oper_log` VALUES (21, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/103', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"部门存在用户,不允许删除\",\"code\":301}', 0, NULL, '2021-01-27 16:29:44');
INSERT INTO `sys_oper_log` VALUES (22, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/107', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:25');
INSERT INTO `sys_oper_log` VALUES (23, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/106', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:28');
INSERT INTO `sys_oper_log` VALUES (24, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/105', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:30');
INSERT INTO `sys_oper_log` VALUES (25, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/103', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:33');
INSERT INTO `sys_oper_log` VALUES (26, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/109', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:35');
INSERT INTO `sys_oper_log` VALUES (27, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/108', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:37');
INSERT INTO `sys_oper_log` VALUES (28, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/102', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:43');
INSERT INTO `sys_oper_log` VALUES (29, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/101', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:36:45');
INSERT INTO `sys_oper_log` VALUES (30, '部门管理', 2, 'com.ruoyi.project.system.dept.controller.DeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"若依科技\"],\"orderNum\":[\"0\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:52:37');
INSERT INTO `sys_oper_log` VALUES (31, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"0\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-27 16:58:42');
INSERT INTO `sys_oper_log` VALUES (32, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:46:41');
INSERT INTO `sys_oper_log` VALUES (33, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:48:04');
INSERT INTO `sys_oper_log` VALUES (34, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/116', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:48:12');
INSERT INTO `sys_oper_log` VALUES (35, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"115\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:48:20');
INSERT INTO `sys_oper_log` VALUES (36, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/117', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:48:40');
INSERT INTO `sys_oper_log` VALUES (37, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/115', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:48:42');
INSERT INTO `sys_oper_log` VALUES (38, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:55:35');
INSERT INTO `sys_oper_log` VALUES (39, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"118\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:55:56');
INSERT INTO `sys_oper_log` VALUES (40, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"118\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:56:02');
INSERT INTO `sys_oper_log` VALUES (41, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"118\"],\"deptName\":[\"红色\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:56:20');
INSERT INTO `sys_oper_log` VALUES (42, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"118\"],\"deptName\":[\"亮度加\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 08:56:37');
INSERT INTO `sys_oper_log` VALUES (43, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"汽车控制\"],\"orderNum\":[\"0\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:00:55');
INSERT INTO `sys_oper_log` VALUES (44, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:01:11');
INSERT INTO `sys_oper_log` VALUES (45, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"118\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:01:37');
INSERT INTO `sys_oper_log` VALUES (46, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"123\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:02:17');
INSERT INTO `sys_oper_log` VALUES (47, '部门管理', 2, 'com.ruoyi.project.system.dept.controller.DeptController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/dept/edit', '221.212.191.138', 'XX XX', '{\"deptId\":[\"112\"],\"parentId\":[\"103\"],\"parentName\":[\"研发部门\"],\"deptName\":[\"111\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:03:17');
INSERT INTO `sys_oper_log` VALUES (48, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"119\"],\"parentId\":[\"123\"],\"parentName\":[\"灯光\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:03:43');
INSERT INTO `sys_oper_log` VALUES (49, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/118', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"存在下级控制功能,不允许删除\",\"code\":301}', 0, NULL, '2021-01-28 09:04:10');
INSERT INTO `sys_oper_log` VALUES (50, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/120', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:17');
INSERT INTO `sys_oper_log` VALUES (51, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/121', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:19');
INSERT INTO `sys_oper_log` VALUES (52, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/122', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:21');
INSERT INTO `sys_oper_log` VALUES (53, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/118', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:23');
INSERT INTO `sys_oper_log` VALUES (54, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/119', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:30');
INSERT INTO `sys_oper_log` VALUES (55, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"124\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:40');
INSERT INTO `sys_oper_log` VALUES (56, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"124\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:48');
INSERT INTO `sys_oper_log` VALUES (57, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"124\"],\"deptName\":[\"红色\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:04:55');
INSERT INTO `sys_oper_log` VALUES (58, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"127\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"红色\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:05:03');
INSERT INTO `sys_oper_log` VALUES (59, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"124\"],\"deptName\":[\"亮度+\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:05:16');
INSERT INTO `sys_oper_log` VALUES (60, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:05:46');
INSERT INTO `sys_oper_log` VALUES (61, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"129\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:05:57');
INSERT INTO `sys_oper_log` VALUES (62, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"129\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 09:06:05');
INSERT INTO `sys_oper_log` VALUES (63, '用户管理', 1, 'com.ruoyi.project.system.user.controller.UserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '221.212.191.138', 'XX XX', '{\"deptId\":[\"103\"],\"userName\":[\"黑AVC815\"],\"deptName\":[\"研发部门\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"001\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-28 11:43:53');
INSERT INTO `sys_oper_log` VALUES (64, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1062\"],\"menuType\":[\"C\"],\"menuName\":[\"车辆管理\"],\"url\":[\"/qiChe/cheLiang\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 10:11:37');
INSERT INTO `sys_oper_log` VALUES (65, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1064\"],\"parentId\":[\"1062\"],\"menuType\":[\"C\"],\"menuName\":[\"车辆管理\"],\"url\":[\"/qiChe/cheLiang\"],\"target\":[\"menuItem\"],\"perms\":[\"qiChe:cheLiang:view\"],\"orderNum\":[\"2\"],\"icon\":[\"#\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 10:12:02');
INSERT INTO `sys_oper_log` VALUES (66, '个人信息', 2, 'com.ruoyi.project.system.user.controller.ProfileController.updateAvatar()', 'POST', 1, 'admin', '研发部门', '/system/user/profile/updateAvatar', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 12:02:42');
INSERT INTO `sys_oper_log` VALUES (67, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"1\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 15:02:28');
INSERT INTO `sys_oper_log` VALUES (68, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"2\"],\"orderNum\":[\"2\"],\"leader\":[\"2\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:14:47');
INSERT INTO `sys_oper_log` VALUES (69, '部门管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"3\"],\"orderNum\":[\"3\"],\"leader\":[\"3\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:16:20');
INSERT INTO `sys_oper_log` VALUES (70, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"5\"],\"orderNum\":[\"3\"],\"leader\":[\"3\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:40:21');
INSERT INTO `sys_oper_log` VALUES (71, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"4\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:52:14');
INSERT INTO `sys_oper_log` VALUES (72, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"6\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:53:00');
INSERT INTO `sys_oper_log` VALUES (73, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/135', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:55:46');
INSERT INTO `sys_oper_log` VALUES (74, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/134', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:55:49');
INSERT INTO `sys_oper_log` VALUES (75, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/133', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:55:52');
INSERT INTO `sys_oper_log` VALUES (76, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"汽车控制\"],\"orderNum\":[\"0\"],\"leader\":[\"\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-01-29 16:59:54');
INSERT INTO `sys_oper_log` VALUES (77, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"6\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 12:05:11');
INSERT INTO `sys_oper_log` VALUES (78, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"6\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 12:05:15');
INSERT INTO `sys_oper_log` VALUES (79, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"6\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 12:05:20');
INSERT INTO `sys_oper_log` VALUES (80, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 12:05:31');
INSERT INTO `sys_oper_log` VALUES (81, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.insertDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_kong_zhi(      dept_id,       parent_id,       dept_name,       ancestors,       order_num,       leader,                   status,       create_by,       tuBiaoLuJing,      tuBiaoDianJiLuJing,      create_time    )values(      ?,       ?,       ?,       ?,       ?,       ?,                   ?,       ?,       ?,      ?,       sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'\n; Duplicate entry \'137\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'', '2021-02-01 13:02:49');
INSERT INTO `sys_oper_log` VALUES (82, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"136\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"4\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.insertDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_kong_zhi(      dept_id,       parent_id,       dept_name,       ancestors,       order_num,       leader,                   status,       create_by,       tuBiaoLuJing,      tuBiaoDianJiLuJing,      create_time    )values(      ?,       ?,       ?,       ?,       ?,       ?,                   ?,       ?,       ?,      ?,       sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'\n; Duplicate entry \'136\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'', '2021-02-01 13:07:29');
INSERT INTO `sys_oper_log` VALUES (83, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"136\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"4\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.insertDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_kong_zhi(      dept_id,       parent_id,       dept_name,       ancestors,       order_num,       leader,                   status,       create_by,       tuBiaoLuJing,      tuBiaoDianJiLuJing,      create_time    )values(      ?,       ?,       ?,       ?,       ?,       ?,                   ?,       ?,       ?,      ?,       sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'\n; Duplicate entry \'136\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'136\' for key \'PRIMARY\'', '2021-02-01 13:07:40');
INSERT INTO `sys_oper_log` VALUES (84, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.insertDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_kong_zhi(      dept_id,       parent_id,       dept_name,       ancestors,       order_num,       leader,                   status,       create_by,       tuBiaoLuJing,      tuBiaoDianJiLuJing,      create_time    )values(      ?,       ?,       ?,       ?,       ?,       ?,                   ?,       ?,       ?,      ?,       sysdate()    )\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'\n; Duplicate entry \'137\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'137\' for key \'PRIMARY\'', '2021-02-01 13:08:37');
INSERT INTO `sys_oper_log` VALUES (85, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"136\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"4\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/6a2dbeec-5dd0-4475-bd12-e23856d6b18f.jpg\',\n			\'/profile//2\' at line 8\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.updateDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update qi_che_kong_zhi     SET parent_id = ?,     dept_name = ?,     ancestors = ?,     order_num = ?,     leader = ?,     ?,    ?,                status = ?,     update_by = ?,     update_time = sysdate()     where dept_id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/6a2dbeec-5dd0-4475-bd12-e23856d6b18f.jpg\',\n			\'/profile//2\' at line 8\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/6a2dbeec-5dd0-4475-bd12-e23856d6b18f.jpg\',\n			\'/profile//2\' at line 8', '2021-02-01 13:10:18');
INSERT INTO `sys_oper_log` VALUES (86, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"136\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"4\"],\"orderNum\":[\"1\"],\"leader\":[\"1\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/83d0fdba-e130-4e9a-8343-7a353acca143.jpg\',\n			\'/profile//2\' at line 8\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.updateDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update qi_che_kong_zhi     SET parent_id = ?,     dept_name = ?,     ancestors = ?,     order_num = ?,     leader = ?,     ?,    ?,                status = ?,     update_by = ?,     update_time = sysdate()     where dept_id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/83d0fdba-e130-4e9a-8343-7a353acca143.jpg\',\n			\'/profile//2\' at line 8\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/profile//2021/02/01/83d0fdba-e130-4e9a-8343-7a353acca143.jpg\',\n			\'/profile//2\' at line 8', '2021-02-01 13:10:18');
INSERT INTO `sys_oper_log` VALUES (87, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.updateDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update qi_che_kong_zhi     SET parent_id = ?,     dept_name = ?,     ancestors = ?,     order_num = ?,     leader = ?,     ?,    ?,                status = ?,     update_by = ?,     update_time = sysdate()     where dept_id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8', '2021-02-01 13:10:24');
INSERT INTO `sys_oper_log` VALUES (88, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.updateDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update qi_che_kong_zhi     SET parent_id = ?,     dept_name = ?,     ancestors = ?,     order_num = ?,     leader = ?,     ?,    ?,                status = ?,     update_by = ?,     update_time = sysdate()     where dept_id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8', '2021-02-01 13:10:31');
INSERT INTO `sys_oper_log` VALUES (89, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\KongZhiMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.kongZhi.mapper.KongZhiMapper.updateDept-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update qi_che_kong_zhi     SET parent_id = ?,     dept_name = ?,     ancestors = ?,     order_num = ?,     leader = ?,     ?,    ?,                status = ?,     update_by = ?,     update_time = sysdate()     where dept_id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\'/img/mo_ren1.png\',\n			\'/img/mo_ren2.png\',\n\n\n			\n 			\n 			status = \'0\',\n 			upda\' at line 8', '2021-02-01 13:11:53');
INSERT INTO `sys_oper_log` VALUES (90, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:14:32');
INSERT INTO `sys_oper_log` VALUES (91, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:14:45');
INSERT INTO `sys_oper_log` VALUES (92, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:14:54');
INSERT INTO `sys_oper_log` VALUES (93, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:15:05');
INSERT INTO `sys_oper_log` VALUES (94, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:15:34');
INSERT INTO `sys_oper_log` VALUES (95, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:17:48');
INSERT INTO `sys_oper_log` VALUES (96, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"123123111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:17:51');
INSERT INTO `sys_oper_log` VALUES (97, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"7\"],\"orderNum\":[\"2\"],\"leader\":[\"321321321321\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:17:58');
INSERT INTO `sys_oper_log` VALUES (98, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"11111\"],\"orderNum\":[\"1111\"],\"leader\":[\"321321321321\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:18:04');
INSERT INTO `sys_oper_log` VALUES (99, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"137\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"11111\"],\"orderNum\":[\"1111\"],\"leader\":[\"321321321321\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:18:14');
INSERT INTO `sys_oper_log` VALUES (100, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"125\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"12313131\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:18:35');
INSERT INTO `sys_oper_log` VALUES (101, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"125\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"001\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:43:54');
INSERT INTO `sys_oper_log` VALUES (102, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"126\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"002\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:03');
INSERT INTO `sys_oper_log` VALUES (103, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"127\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"红色\"],\"orderNum\":[\"3\"],\"leader\":[\"003\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:15');
INSERT INTO `sys_oper_log` VALUES (104, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"128\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"亮度+\"],\"orderNum\":[\"4\"],\"leader\":[\"004\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:22');
INSERT INTO `sys_oper_log` VALUES (105, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:27');
INSERT INTO `sys_oper_log` VALUES (106, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"130\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"006\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:32');
INSERT INTO `sys_oper_log` VALUES (107, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"131\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"007\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:37');
INSERT INTO `sys_oper_log` VALUES (108, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/132', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:42');
INSERT INTO `sys_oper_log` VALUES (109, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/136', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:44');
INSERT INTO `sys_oper_log` VALUES (110, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/137', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:44:49');
INSERT INTO `sys_oper_log` VALUES (111, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"130\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"006\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:45:17');
INSERT INTO `sys_oper_log` VALUES (112, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"131\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"007\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:45:28');
INSERT INTO `sys_oper_log` VALUES (113, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"131\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"007\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 13:45:31');
INSERT INTO `sys_oper_log` VALUES (114, '角色管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"111\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:21:27');
INSERT INTO `sys_oper_log` VALUES (115, '角色管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"111\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:21:35');
INSERT INTO `sys_oper_log` VALUES (116, '角色管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"111\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:22:06');
INSERT INTO `sys_oper_log` VALUES (117, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:27:43');
INSERT INTO `sys_oper_log` VALUES (118, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:29:30');
INSERT INTO `sys_oper_log` VALUES (119, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-01 15:54:41');
INSERT INTO `sys_oper_log` VALUES (120, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"1111\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'role_key\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\CheLiangMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.cheLiang.mapper.CheLiangMapper.insertRole-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_che_liang(            role_name,                         status,             create_by,      create_time    )values(            ?,                         ?,             ?,      sysdate()    )\r\n### Cause: java.sql.SQLException: Field \'role_key\' doesn\'t have a default value\n; Field \'role_key\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'role_key\' doesn\'t have a default value', '2021-02-01 15:55:51');
INSERT INTO `sys_oper_log` VALUES (121, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"1111\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'role_key\' doesn\'t have a default value\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\CheLiangMapper.xml]\r\n### The error may involve com.ruoyi.project.qiChe.cheLiang.mapper.CheLiangMapper.insertRole-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_che_liang(            role_name,                         status,             create_by,      create_time    )values(            ?,                         ?,             ?,      sysdate()    )\r\n### Cause: java.sql.SQLException: Field \'role_key\' doesn\'t have a default value\n; Field \'role_key\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'role_key\' doesn\'t have a default value', '2021-02-01 15:55:57');
INSERT INTO `sys_oper_log` VALUES (122, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"1111\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\CheLiangKongZhiMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_che_laing_kong_zhi(role_id, dept_id) values         (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)\r\n### Cause: java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist', '2021-02-01 15:57:23');
INSERT INTO `sys_oper_log` VALUES (123, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"1111\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"新增车辆\'1111\'失败，车辆名称已存在\",\"code\":500}', 0, NULL, '2021-02-01 15:57:28');
INSERT INTO `sys_oper_log` VALUES (124, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"2222\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist\r\n### The error may exist in file [D:\\workspace\\qi_che\\RuoYi-fast\\target\\classes\\mybatis\\qiChe\\CheLiangKongZhiMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: insert into qi_che_che_laing_kong_zhi(role_id, dept_id) values         (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)    ,     (?,?)\r\n### Cause: java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Table \'ry.qi_che_che_laing_kong_zhi\' doesn\'t exist', '2021-02-01 15:57:34');
INSERT INTO `sys_oper_log` VALUES (125, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"333\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 15:59:29');
INSERT INTO `sys_oper_log` VALUES (126, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"111\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 16:18:01');
INSERT INTO `sys_oper_log` VALUES (127, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"333\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"新增车辆\'333\'失败，车辆名称已存在\",\"code\":500}', 0, NULL, '2021-02-01 16:25:42');
INSERT INTO `sys_oper_log` VALUES (128, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"444\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-01 16:25:47');
INSERT INTO `sys_oper_log` VALUES (129, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', 'null', 1, '不允许操作超级管理员角色', '2021-02-02 09:48:19');
INSERT INTO `sys_oper_log` VALUES (130, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"黑\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 09:49:43');
INSERT INTO `sys_oper_log` VALUES (131, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"黑\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 09:49:52');
INSERT INTO `sys_oper_log` VALUES (132, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 09:50:10');
INSERT INTO `sys_oper_log` VALUES (133, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 09:50:16');
INSERT INTO `sys_oper_log` VALUES (134, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"123\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 09:56:40');
INSERT INTO `sys_oper_log` VALUES (135, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 09:57:59');
INSERT INTO `sys_oper_log` VALUES (136, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 09:58:06');
INSERT INTO `sys_oper_log` VALUES (137, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"1\"],\"roleName\":[\"黑1234\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,131\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:04:17');
INSERT INTO `sys_oper_log` VALUES (138, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"6\"],\"roleName\":[\"黑88888\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:04:36');
INSERT INTO `sys_oper_log` VALUES (139, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:04:39');
INSERT INTO `sys_oper_log` VALUES (140, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:04:50');
INSERT INTO `sys_oper_log` VALUES (141, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"3\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:13:35');
INSERT INTO `sys_oper_log` VALUES (142, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"333,444,2222\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:13:40');
INSERT INTO `sys_oper_log` VALUES (143, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"333,444,2222\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:13:50');
INSERT INTO `sys_oper_log` VALUES (144, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"333,444,2222\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:14:30');
INSERT INTO `sys_oper_log` VALUES (145, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:15:12');
INSERT INTO `sys_oper_log` VALUES (146, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"333,444\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:16:18');
INSERT INTO `sys_oper_log` VALUES (147, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"7\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:21:58');
INSERT INTO `sys_oper_log` VALUES (148, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"1111111\"],\"status\":[\"1\"],\"menuIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:22:08');
INSERT INTO `sys_oper_log` VALUES (149, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"333,1111111\"]}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2021-02-02 10:22:34');
INSERT INTO `sys_oper_log` VALUES (150, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"5,8\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:28:07');
INSERT INTO `sys_oper_log` VALUES (151, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/131', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:28:39');
INSERT INTO `sys_oper_log` VALUES (152, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"129\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:29:30');
INSERT INTO `sys_oper_log` VALUES (153, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130,138\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:29:43');
INSERT INTO `sys_oper_log` VALUES (154, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"4\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"若依官网\"],\"url\":[\"http://ruoyi.vip\"],\"target\":[\"menuBlank\"],\"perms\":[\"\"],\"orderNum\":[\"4\"],\"icon\":[\"fa fa-location-arrow\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:33:20');
INSERT INTO `sys_oper_log` VALUES (155, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"3\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"系统工具\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"3\"],\"icon\":[\"fa fa-bars\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:33:38');
INSERT INTO `sys_oper_log` VALUES (156, '菜单管理', 2, 'com.ruoyi.project.system.menu.controller.MenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2\"],\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"系统监控\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"fa fa-video-camera\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-02 10:34:10');
INSERT INTO `sys_oper_log` VALUES (157, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '1.58.195.229', 'XX XX', '{\"deptId\":[\"123\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-03 18:53:35');
INSERT INTO `sys_oper_log` VALUES (158, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '1.58.195.229', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-05 00:40:30');
INSERT INTO `sys_oper_log` VALUES (159, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '1.58.195.229', 'XX XX', '{\"roleId\":[\"6\"],\"roleName\":[\"黑88888\"],\"status\":[\"1\"],\"menuIds\":[\"100,129,130,138\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-05 00:40:41');
INSERT INTO `sys_oper_log` VALUES (160, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '1.58.195.229', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,123,124,125,126,127,128,129,130\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-05 00:41:10');
INSERT INTO `sys_oper_log` VALUES (161, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-06 14:41:14');
INSERT INTO `sys_oper_log` VALUES (162, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"123\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-06 14:49:16');
INSERT INTO `sys_oper_log` VALUES (163, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-06 14:50:08');
INSERT INTO `sys_oper_log` VALUES (164, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-06 14:50:30');
INSERT INTO `sys_oper_log` VALUES (165, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:34:30');
INSERT INTO `sys_oper_log` VALUES (166, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:34:33');
INSERT INTO `sys_oper_log` VALUES (167, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:37:41');
INSERT INTO `sys_oper_log` VALUES (168, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:37:51');
INSERT INTO `sys_oper_log` VALUES (169, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:38:06');
INSERT INTO `sys_oper_log` VALUES (170, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:38:25');
INSERT INTO `sys_oper_log` VALUES (171, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:38:31');
INSERT INTO `sys_oper_log` VALUES (172, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', 'null', 1, '', '2021-02-07 17:38:37');
INSERT INTO `sys_oper_log` VALUES (173, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"005\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 17:39:51');
INSERT INTO `sys_oper_log` VALUES (174, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 17:39:56');
INSERT INTO `sys_oper_log` VALUES (175, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"123\"],\"orderNum\":[\"123\"],\"leader\":[\"123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 17:44:09');
INSERT INTO `sys_oper_log` VALUES (176, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"139\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"123\"],\"orderNum\":[\"123\"],\"leader\":[\"123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 17:44:15');
INSERT INTO `sys_oper_log` VALUES (177, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"139\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"123\"],\"orderNum\":[\"123\"],\"leader\":[\"123\"],\"status\":[\"0\"]}', 'null', 1, 'java.io.FileNotFoundException: C:\\Users\\Administrator\\AppData\\Local\\Temp\\tomcat.80.805668591544857481\\work\\Tomcat\\localhost\\ROOT\\usr\\qiChe\\uploadPath\\2021\\02\\07\\cd4710c1-ef0a-4bc7-8f3a-50d1055b870e.jpg (系统找不到指定的路径。)', '2021-02-07 17:44:21');
INSERT INTO `sys_oper_log` VALUES (178, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"139\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"123\"],\"orderNum\":[\"123\"],\"leader\":[\"123\"],\"status\":[\"0\"]}', 'null', 1, 'java.io.FileNotFoundException: C:\\Users\\Administrator\\AppData\\Local\\Temp\\tomcat.80.805668591544857481\\work\\Tomcat\\localhost\\ROOT\\usr\\qiChe\\uploadPath\\2021\\02\\07\\e925fd07-99bf-457a-a644-603c0e1ba1a5.jpg (系统找不到指定的路径。)', '2021-02-07 17:44:30');
INSERT INTO `sys_oper_log` VALUES (179, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 17:52:43');
INSERT INTO `sys_oper_log` VALUES (180, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"123\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:18');
INSERT INTO `sys_oper_log` VALUES (181, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"124\"],\"parentId\":[\"123\"],\"parentName\":[\"灯光\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:21');
INSERT INTO `sys_oper_log` VALUES (182, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"125\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"001\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:22');
INSERT INTO `sys_oper_log` VALUES (183, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"125\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"001\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:24');
INSERT INTO `sys_oper_log` VALUES (184, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"126\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"002\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:26');
INSERT INTO `sys_oper_log` VALUES (185, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"127\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"红色\"],\"orderNum\":[\"3\"],\"leader\":[\"003\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:27');
INSERT INTO `sys_oper_log` VALUES (186, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"128\"],\"parentId\":[\"124\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"亮度+\"],\"orderNum\":[\"4\"],\"leader\":[\"004\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:29');
INSERT INTO `sys_oper_log` VALUES (187, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"129\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:30');
INSERT INTO `sys_oper_log` VALUES (188, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"130\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"006\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:32');
INSERT INTO `sys_oper_log` VALUES (189, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"138\"],\"parentId\":[\"129\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:33');
INSERT INTO `sys_oper_log` VALUES (190, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"139\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"123\"],\"orderNum\":[\"123\"],\"leader\":[\"123\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:17:35');
INSERT INTO `sys_oper_log` VALUES (191, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/139', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:14');
INSERT INTO `sys_oper_log` VALUES (192, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/138', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:17');
INSERT INTO `sys_oper_log` VALUES (193, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/130', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:19');
INSERT INTO `sys_oper_log` VALUES (194, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/129', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:21');
INSERT INTO `sys_oper_log` VALUES (195, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/128', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:22');
INSERT INTO `sys_oper_log` VALUES (196, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/127', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:24');
INSERT INTO `sys_oper_log` VALUES (197, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/126', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:25');
INSERT INTO `sys_oper_log` VALUES (198, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/125', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:27');
INSERT INTO `sys_oper_log` VALUES (199, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/124', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:29');
INSERT INTO `sys_oper_log` VALUES (200, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/123', '125.211.31.149', 'XX XX', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:32');
INSERT INTO `sys_oper_log` VALUES (201, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"100\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:18:43');
INSERT INTO `sys_oper_log` VALUES (202, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"140\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:23:20');
INSERT INTO `sys_oper_log` VALUES (203, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"140\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:25:45');
INSERT INTO `sys_oper_log` VALUES (204, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"140\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:27:48');
INSERT INTO `sys_oper_log` VALUES (205, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '125.211.31.149', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,140\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:33:58');
INSERT INTO `sys_oper_log` VALUES (206, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"140\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:34:18');
INSERT INTO `sys_oper_log` VALUES (207, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"140\"],\"deptName\":[\"氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:34:52');
INSERT INTO `sys_oper_log` VALUES (208, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"100\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:35:09');
INSERT INTO `sys_oper_log` VALUES (209, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '125.211.31.149', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,142\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 18:35:16');
INSERT INTO `sys_oper_log` VALUES (210, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"141\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:56:26');
INSERT INTO `sys_oper_log` VALUES (211, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"141\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:56:33');
INSERT INTO `sys_oper_log` VALUES (212, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"140\"],\"deptName\":[\"下氛围灯\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:56:59');
INSERT INTO `sys_oper_log` VALUES (213, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"141\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:57:26');
INSERT INTO `sys_oper_log` VALUES (214, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"141\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:57:37');
INSERT INTO `sys_oper_log` VALUES (215, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"143\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:57:51');
INSERT INTO `sys_oper_log` VALUES (216, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"143\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:58:03');
INSERT INTO `sys_oper_log` VALUES (217, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"142\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:58:28');
INSERT INTO `sys_oper_log` VALUES (218, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '125.211.31.149', 'XX XX', '{\"parentId\":[\"142\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:58:47');
INSERT INTO `sys_oper_log` VALUES (219, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"144\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:02');
INSERT INTO `sys_oper_log` VALUES (220, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"145\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"222\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:05');
INSERT INTO `sys_oper_log` VALUES (221, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"146\"],\"parentId\":[\"143\"],\"parentName\":[\"下氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"333\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:09');
INSERT INTO `sys_oper_log` VALUES (222, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"147\"],\"parentId\":[\"143\"],\"parentName\":[\"下氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"444\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:12');
INSERT INTO `sys_oper_log` VALUES (223, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"148\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:15');
INSERT INTO `sys_oper_log` VALUES (224, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"149\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"666\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:18');
INSERT INTO `sys_oper_log` VALUES (225, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '125.211.31.149', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:33');
INSERT INTO `sys_oper_log` VALUES (226, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"142\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 20:59:58');
INSERT INTO `sys_oper_log` VALUES (227, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"141\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 21:00:14');
INSERT INTO `sys_oper_log` VALUES (228, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"148\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 21:00:37');
INSERT INTO `sys_oper_log` VALUES (229, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '125.211.31.149', 'XX XX', '{\"deptId\":[\"149\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"666\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-02-07 21:00:42');
INSERT INTO `sys_oper_log` VALUES (230, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"140\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"灯光\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:29');
INSERT INTO `sys_oper_log` VALUES (231, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"141\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:33');
INSERT INTO `sys_oper_log` VALUES (232, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"144\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:36');
INSERT INTO `sys_oper_log` VALUES (233, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"145\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"222\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:38');
INSERT INTO `sys_oper_log` VALUES (234, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"143\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"下氛围灯\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:44');
INSERT INTO `sys_oper_log` VALUES (235, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"147\"],\"parentId\":[\"143\"],\"parentName\":[\"下氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"444\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:47');
INSERT INTO `sys_oper_log` VALUES (236, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"142\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:50');
INSERT INTO `sys_oper_log` VALUES (237, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"148\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:52');
INSERT INTO `sys_oper_log` VALUES (238, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"148\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:54');
INSERT INTO `sys_oper_log` VALUES (239, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"149\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"666\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:12:56');
INSERT INTO `sys_oper_log` VALUES (240, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"141\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"上氛围灯\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:14:30');
INSERT INTO `sys_oper_log` VALUES (241, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"144\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"111\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:16:28');
INSERT INTO `sys_oper_log` VALUES (242, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"145\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"222\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:16:36');
INSERT INTO `sys_oper_log` VALUES (243, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"143\"],\"parentId\":[\"140\"],\"parentName\":[\"灯光\"],\"deptName\":[\"下氛围灯\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:16:43');
INSERT INTO `sys_oper_log` VALUES (244, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"146\"],\"parentId\":[\"143\"],\"parentName\":[\"下氛围灯\"],\"deptName\":[\"开\"],\"orderNum\":[\"1\"],\"leader\":[\"333\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:16:49');
INSERT INTO `sys_oper_log` VALUES (245, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"147\"],\"parentId\":[\"143\"],\"parentName\":[\"下氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"444\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:16:55');
INSERT INTO `sys_oper_log` VALUES (246, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"142\"],\"parentId\":[\"100\"],\"parentName\":[\"汽车控制\"],\"deptName\":[\"座椅\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:18:06');
INSERT INTO `sys_oper_log` VALUES (247, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"148\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向前\"],\"orderNum\":[\"1\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:18:13');
INSERT INTO `sys_oper_log` VALUES (248, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"149\"],\"parentId\":[\"142\"],\"parentName\":[\"座椅\"],\"deptName\":[\"向后\"],\"orderNum\":[\"2\"],\"leader\":[\"666\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:18:19');
INSERT INTO `sys_oper_log` VALUES (249, '用户管理', 1, 'com.ruoyi.project.system.user.controller.UserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '43.225.208.38', 'XX XX', '{\"deptId\":[\"\"],\"userName\":[\"黑AS32A5\"],\"deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"admin\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '{\"msg\":\"新增用户\'admin\'失败，登录账号已存在\",\"code\":500}', 0, NULL, '2021-04-01 20:44:02');
INSERT INTO `sys_oper_log` VALUES (250, '用户管理', 1, 'com.ruoyi.project.system.user.controller.UserController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/user/add', '43.225.208.38', 'XX XX', '{\"deptId\":[\"\"],\"userName\":[\"黑AS32A5\"],\"deptName\":[\"\"],\"phonenumber\":[\"\"],\"email\":[\"\"],\"loginName\":[\"zheng\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:44:10');
INSERT INTO `sys_oper_log` VALUES (251, '用户管理', 3, 'com.ruoyi.project.system.user.controller.UserController.remove()', 'POST', 1, 'admin', '研发部门', '/system/user/remove', '43.225.208.38', 'XX XX', '{\"ids\":[\"4\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:45:14');
INSERT INTO `sys_oper_log` VALUES (252, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '43.225.208.38', 'XX XX', '{\"deptId\":[\"145\"],\"parentId\":[\"141\"],\"parentName\":[\"上氛围灯\"],\"deptName\":[\"关\"],\"orderNum\":[\"2\"],\"leader\":[\"222\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:46:16');
INSERT INTO `sys_oper_log` VALUES (253, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '43.225.208.38', 'XX XX', '{\"roleId\":[\"6\"],\"roleName\":[\"黑88888\"],\"status\":[\"1\"],\"menuIds\":[\"100\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:47:46');
INSERT INTO `sys_oper_log` VALUES (254, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '43.225.208.38', 'XX XX', '{\"roleName\":[\"黑AS1234\"],\"status\":[\"1\"],\"menuIds\":[\"100,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:48:07');
INSERT INTO `sys_oper_log` VALUES (255, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '43.225.208.38', 'XX XX', '{\"roleId\":[\"9\"],\"roleName\":[\"黑AS1234\"],\"status\":[\"1\"],\"menuIds\":[\"100,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-01 20:48:15');
INSERT INTO `sys_oper_log` VALUES (256, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '43.225.208.71', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-07 21:46:12');
INSERT INTO `sys_oper_log` VALUES (257, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '43.225.208.71', 'XX XX', '{\"roleId\":[\"2\"],\"roleName\":[\"黑AVC815\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-07 21:53:17');
INSERT INTO `sys_oper_log` VALUES (258, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"6\"],\"roleName\":[\"黑88888\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-09 14:48:51');
INSERT INTO `sys_oper_log` VALUES (259, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"黑AS32A5\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-11 22:00:40');
INSERT INTO `sys_oper_log` VALUES (260, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"黑ACD123\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-04-12 15:18:33');
INSERT INTO `sys_oper_log` VALUES (261, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"苏D1234\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-22 13:48:00');
INSERT INTO `sys_oper_log` VALUES (262, '角色管理', 3, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.remove()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"12\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-22 14:06:00');
INSERT INTO `sys_oper_log` VALUES (263, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"苏D12345\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-22 14:06:45');
INSERT INTO `sys_oper_log` VALUES (264, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"苏D4444\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-24 11:25:29');
INSERT INTO `sys_oper_log` VALUES (265, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"电视\"],\"orderNum\":[\"3\"],\"leader\":[\"555\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-24 11:31:50');
INSERT INTO `sys_oper_log` VALUES (266, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"14\"],\"roleName\":[\"苏D4444\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149,150\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-24 11:32:02');
INSERT INTO `sys_oper_log` VALUES (267, '车辆管理', 1, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"模式2\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,143,146,147,142,148,149,150\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:51:24');
INSERT INTO `sys_oper_log` VALUES (268, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"空调\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:51:39');
INSERT INTO `sys_oper_log` VALUES (269, '角色管理', 2, 'com.ruoyi.project.qiChe.cheLiang.controller.CheLiangController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/cheLiang/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"11\"],\"roleName\":[\"123456567\"],\"status\":[\"1\"],\"menuIds\":[\"100,140,141,144,145,143,146,147,142,148,149\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:51:52');
INSERT INTO `sys_oper_log` VALUES (270, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"空调1挡\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:52:02');
INSERT INTO `sys_oper_log` VALUES (271, '部门管理', 3, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.remove()', 'GET', 1, 'admin', '研发部门', '/qiChe/kongZhi/remove/152', '127.0.0.1', '内网IP', NULL, '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:52:18');
INSERT INTO `sys_oper_log` VALUES (272, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"151\"],\"deptName\":[\"空调1挡\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:52:35');
INSERT INTO `sys_oper_log` VALUES (273, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"151\"],\"deptName\":[\"空调2挡\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:52:54');
INSERT INTO `sys_oper_log` VALUES (274, '控制管理', 1, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.addSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"151\"],\"deptName\":[\"空调3挡\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 13:53:10');
INSERT INTO `sys_oper_log` VALUES (275, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"153\"],\"parentId\":[\"151\"],\"parentName\":[\"空调\"],\"deptName\":[\"空调1挡\"],\"orderNum\":[\"1\"],\"leader\":[\"8888\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 14:49:01');
INSERT INTO `sys_oper_log` VALUES (276, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"154\"],\"parentId\":[\"151\"],\"parentName\":[\"空调\"],\"deptName\":[\"空调2挡\"],\"orderNum\":[\"2\"],\"leader\":[\"999\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 14:49:07');
INSERT INTO `sys_oper_log` VALUES (277, '部门管理', 2, 'com.ruoyi.project.qiChe.kongZhi.controller.KongZhiController.editSave()', 'POST', 1, 'admin', '研发部门', '/qiChe/kongZhi/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"155\"],\"parentId\":[\"151\"],\"parentName\":[\"空调\"],\"deptName\":[\"空调3挡\"],\"orderNum\":[\"3\"],\"leader\":[\"aaa\"],\"status\":[\"0\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2021-05-25 14:49:14');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-01-26 16:25:42', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-01-26 16:25:42', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-01-26 16:25:43', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-01-26 16:25:43', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2021-01-26 16:25:45', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2021-01-26 16:25:45', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '/profile/avatar/2021/01/29/1a250c68-99d6-4710-8f69-e23480c7cc94.png', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2021-05-25 13:48:11', '2021-01-26 16:25:40', 'admin', '2021-01-26 16:25:40', '', '2021-05-25 13:48:10', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2021-01-26 16:25:40', '2021-01-26 16:25:40', 'admin', '2021-01-26 16:25:40', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (3, 103, '001', '黑AVC815', '00', '', '', '0', '', '37b873727f494b170a9c67e809ef6c78', '235d9d', '0', '0', '', NULL, NULL, 'admin', '2021-01-28 11:43:53', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (4, NULL, 'zheng', '黑AS32A5', '00', '', '', '0', '', '5cfc86348247472f0203e92fc1090d4f', 'b8f1ad', '0', '2', '', NULL, NULL, 'admin', '2021-04-01 20:44:10', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
