package com.ruoyi.project.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.common.mapper.CommonZwlMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
@Controller
@RequestMapping("/commonZwl")
public class CommonZwlController
{
    //@Autowired
    //private UserMapper userMapper;

    @Autowired
    private CommonZwlMapper commonZwlMapper;

    @ResponseBody
    @RequestMapping("/huoquOpenid")
    public String getUserInfo(@RequestParam(name= "code")String code) throws Exception {
        Map<String,Object> rtnMap = new HashMap<String,Object>();
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        url += "?appid=wx2d050228ef6b9746";//自己的appid
        url += "&secret=ed3d47950d69250fa50b0c9773847c1f";//密匙
        url += "&js_code=" + code;
        url += "&grant_type=authorization_code";
        byte[] res = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
            res = IOUtils.toByteArray(response.getEntity().getContent());
        } catch (Exception e) {
            throw e;
        } finally {
            if (httpget != null) {
                httpget.abort();
            }
            httpclient.getConnectionManager().shutdown();
        }
        JSONObject jo = JSON.parseObject(new String(res, "utf-8"));
        String openid = jo.getString("openid");

        System.out.println(openid);

        return jo.toString();
    }


    /**
     */
    @GetMapping("/getZwl")
    @ResponseBody
    public AjaxResult getZwl(HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            String sql=request.getParameter("sql");
            if(sql.contains("delete")||sql.contains("drop")){
                if(!sql.contains("select")||!sql.contains("insert")||!sql.contains("update")){
                    return AjaxResult.error();
                }
            }
            HashMap map=new HashMap();
            map.put("sql",sql);
            List rtn=new ArrayList();
            if(sql.contains("select")){
                rtn=commonZwlMapper.list(map);
            }
            if(sql.contains("update")){
                rtn.add(commonZwlMapper.update(map));
            }
            if(sql.contains("insert")){
                rtn.add(commonZwlMapper.insert(map));
            }
            AjaxResult ajax = AjaxResult.success();
            ajax.put("rtn", rtn);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     */
    @PostMapping("/postZwl")
    @ResponseBody
    public AjaxResult postTest(HttpServletResponse response, HttpServletRequest request) throws Exception
    {
        try
        {
            AjaxResult ajax = AjaxResult.success();
            ajax.put("postTest", "postTest");
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }


}
