package com.ruoyi.project.common.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CommonZwlMapper {
    List<HashMap<Object, Object>> list(Map sql);
    int insert(Map sql);
    int update(Map sql);
    int delete(Map sql);
}