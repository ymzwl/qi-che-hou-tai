package com.ruoyi.project.qiChe.cheLiang.mapper;


import com.ruoyi.project.qiChe.cheLiang.domain.CheLiang;
import com.ruoyi.project.qiChe.kongZhi.domain.KongZhi;

import java.util.List;

/**
 * 角色表 数据层
 * 
 * @author ruoyi
 */
public interface CheLiangMapper
{

    /**
     * 根据用户ID查询菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<KongZhi> selectMenuAllByUserId(Long userId);

    /**
     * 查询系统所有菜单（含按钮）
     *
     * @return 菜单列表
     */
    public List<KongZhi> selectMenuAll();

    /**
     * 根据角色ID查询菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<String> selectMenuTree(Long roleId);


    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<CheLiang> selectRoleList(CheLiang role);

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<CheLiang> selectRolesByUserId(Long userId);

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public CheLiang selectRoleById(Long roleId);

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleById(Long roleId);

    /**
     * 批量角色用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleByIds(Long[] ids);

    /**
     * 修改角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(CheLiang role);

    /**
     * 新增角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(CheLiang role);

    /**
     * 校验角色名称是否唯一
     * 
     * @param roleName 角色名称
     * @return 角色信息
     */
    public CheLiang checkRoleNameUnique(String roleName);
    
    /**
     * 校验角色权限是否唯一
     * 
     * @param roleKey 角色权限
     * @return 角色信息
     */
    public CheLiang checkRoleKeyUnique(String roleKey);
}
