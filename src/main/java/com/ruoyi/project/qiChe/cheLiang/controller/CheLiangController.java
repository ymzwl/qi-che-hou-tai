package com.ruoyi.project.qiChe.cheLiang.controller;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.security.AuthorizationUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.qiChe.cheLiang.domain.CheLiang;
import com.ruoyi.project.qiChe.cheLiang.service.ICheLiangService;
import com.ruoyi.project.system.role.domain.Role;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.domain.UserRole;
import com.ruoyi.project.system.user.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色信息
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/qiChe/cheLiang")
public class CheLiangController extends BaseController
{
    private String prefix = "qiChe/cheLiang";

    @Autowired
    private ICheLiangService roleService;

    @Autowired
    private IUserService userService;

    @RequiresPermissions("qiChe:cheLiang:view")
    @GetMapping()
    public String cheLiang()
    {
        return prefix + "/cheLiang";
    }

    @RequiresPermissions("qiChe:cheLiang:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CheLiang cheLiang)
    {
        startPage();
        List<CheLiang> list = roleService.selectRoleList(cheLiang);
        return getDataTable(list);
    }

    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("qiChe:cheLiang:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CheLiang cheLiang)
    {
        List<CheLiang> list = roleService.selectRoleList(cheLiang);
        ExcelUtil<CheLiang> util = new ExcelUtil<CheLiang>(CheLiang.class);
        return util.exportExcel(list, "角色数据");
    }


    /**
     * 加载 车辆 控制 列表树
     */
    @GetMapping("/cheLiangKongZhiTreeData")
    @ResponseBody
    public List<Ztree> cheLiangKongZhiTreeData(CheLiang role)
    {
        List<Ztree> ztrees = roleService.cheLiangKongZhiTreeData(role);
        return ztrees;
    }


    /**
     * 新增角色
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存角色
     */
    @RequiresPermissions("qiChe:cheLiang:add")
    @Log(title = "车辆管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated CheLiang cheLiang)
    {
        if (UserConstants.ROLE_NAME_NOT_UNIQUE.equals(roleService.checkRoleNameUnique(cheLiang)))
        {
            return error("新增车辆'" + cheLiang.getRoleName() + "'失败，车辆名称已存在");
        }
        return toAjax(roleService.insertRole(cheLiang));

    }

    /**
     * 修改角色
     */
    @GetMapping("/edit/{roleId}")
    public String edit(@PathVariable("roleId") Long roleId, ModelMap mmap)
    {
        mmap.put("cheLiang", roleService.selectRoleById(roleId));
        return prefix + "/edit";
    }

    /**
     * 修改保存角色
     */
    @RequiresPermissions("qiChe:cheLiang:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated CheLiang cheLiang)
    {
        //roleService.checkRoleAllowed(cheLiang);
        if (UserConstants.ROLE_NAME_NOT_UNIQUE.equals(roleService.checkRoleNameUnique(cheLiang)))
        {
            return error("修改模式'" + cheLiang.getRoleName() + "'失败，车辆名称已存在");
        }
//        else if (UserConstants.ROLE_KEY_NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(cheLiang)))
//        {
//            return error("修改角色'" + cheLiang.getRoleName() + "'失败，角色权限已存在");
//        }
        //AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(roleService.updateRole(cheLiang));
    }

    /**
     * 角色分配数据权限
     */
    @GetMapping("/authDataScope/{roleId}")
    public String authDataScope(@PathVariable("roleId") Long roleId, ModelMap mmap)
    {
        mmap.put("cheLiang", roleService.selectRoleById(roleId));
        return prefix + "/dataScope";
    }

    /**
     * 保存角色分配数据权限
     */
    @RequiresPermissions("qiChe:cheLiang:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authDataScope")
    @ResponseBody
    public AjaxResult authDataScopeSave(CheLiang cheLiang)
    {
        roleService.checkRoleAllowed(cheLiang);
        if (roleService.authDataScope(cheLiang) > 0)
        {
            setSysUser(userService.selectUserById(getSysUser().getUserId()));
            return success();
        }
        return error();
    }

    @RequiresPermissions("qiChe:cheLiang:remove")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(roleService.deleteRoleByIds(ids));
    }

    /**
     * 校验角色名称
     */
    @PostMapping("/checkRoleNameUnique")
    @ResponseBody
    public String checkRoleNameUnique(CheLiang cheLiang)
    {
        return roleService.checkRoleNameUnique(cheLiang);
    }

    /**
     * 校验角色权限
     */
    @PostMapping("/checkRoleKeyUnique")
    @ResponseBody
    public String checkRoleKeyUnique(CheLiang cheLiang)
    {
        return roleService.checkRoleKeyUnique(cheLiang);
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree")
    public String selectMenuTree()
    {
        return prefix + "/tree";
    }

    /**
     * 角色状态修改
     */
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("qiChe:cheLiang:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(CheLiang cheLiang)
    {
        roleService.checkRoleAllowed(cheLiang);
        return toAjax(roleService.changeStatus(cheLiang));
    }

    /**
     * 分配用户
     */
    @RequiresPermissions("qiChe:cheLiang:edit")
    @GetMapping("/authUser/{roleId}")
    public String authUser(@PathVariable("roleId") Long roleId, ModelMap mmap)
    {
        mmap.put("cheLiang", roleService.selectRoleById(roleId));
        return prefix + "/authUser";
    }

    /**
     * 查询已分配用户角色列表
     */
    @RequiresPermissions("qiChe:cheLiang:list")
    @PostMapping("/authUser/allocatedList")
    @ResponseBody
    public TableDataInfo allocatedList(User user)
    {
        startPage();
        List<User> list = userService.selectAllocatedList(user);
        return getDataTable(list);
    }





    /**
     * 选择用户
     */
    @GetMapping("/authUser/selectUser/{roleId}")
    public String selectUser(@PathVariable("roleId") Long roleId, ModelMap mmap)
    {
        mmap.put("cheLiang", roleService.selectRoleById(roleId));
        return prefix + "/selectUser";
    }

    /**
     * 查询未分配用户角色列表
     */
    @RequiresPermissions("qiChe:cheLiang:list")
    @PostMapping("/authUser/unallocatedList")
    @ResponseBody
    public TableDataInfo unallocatedList(User user)
    {
        startPage();
        List<User> list = userService.selectUnallocatedList(user);
        return getDataTable(list);
    }


}