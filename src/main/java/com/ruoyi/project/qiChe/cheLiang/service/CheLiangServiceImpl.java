package com.ruoyi.project.qiChe.cheLiang.service;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.DataScope;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.project.qiChe.cheLiang.domain.CheLiang;
import com.ruoyi.project.qiChe.cheLiang.domain.CheLiangKongZhi;
import com.ruoyi.project.qiChe.cheLiang.domain.CheLiangMenu;
import com.ruoyi.project.qiChe.cheLiang.mapper.CheLiangKongZhiMapper;
import com.ruoyi.project.qiChe.cheLiang.mapper.CheLiangMapper;
import com.ruoyi.project.qiChe.cheLiang.mapper.CheLiangMenuMapper;
import com.ruoyi.project.qiChe.kongZhi.domain.KongZhi;
import com.ruoyi.project.system.role.domain.Role;
import com.ruoyi.project.system.role.domain.RoleMenu;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.domain.UserRole;
import com.ruoyi.project.system.user.mapper.UserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 角色 业务层处理
 *
 * @author ruoyi
 */
@Service
public class CheLiangServiceImpl implements ICheLiangService
{
    @Autowired
    private CheLiangMapper roleMapper;

    @Autowired
    private CheLiangKongZhiMapper roleDeptMapper;

    @Override
    public List<Ztree> cheLiangKongZhiTreeData(CheLiang role) {
        Long roleId = role.getRoleId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<KongZhi> menuList = selectMenuAll();
        if (StringUtils.isNotNull(roleId))
        {
            List<String> roleMenuList = roleMapper.selectMenuTree(roleId);
            ztrees = initZtree(menuList, roleMenuList, true);
        }
        else
        {
            ztrees = initZtree(menuList, null, true);
        }
        return ztrees;
    }
    /**
     * 查询菜单集合
     *
     * @return 所有菜单信息
     */
    @Override
    public List<KongZhi> selectMenuAll()
    {
        List<KongZhi> menuList = null;
        User user = ShiroUtils.getSysUser();
        menuList = roleMapper.selectMenuAll();
        return menuList;
    }

    /**
     * 对象转菜单树
     *
     * @param menuList 菜单列表
     * @param roleMenuList 角色已存在菜单列表
     * @param permsFlag 是否需要显示权限标识
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<KongZhi> menuList, List<String> roleMenuList, boolean permsFlag)
    {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleMenuList);
        for (KongZhi menu : menuList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(menu.getDeptId());
            ztree.setpId(menu.getParentId());
            ztree.setName(menu.getDeptName());
            ztree.setTitle(menu.getDeptName());
            if (isCheck)
            {
                ztree.setChecked(roleMenuList.contains(menu.getDeptId()));
            }
            ztrees.add(ztree);
        }
        return ztrees;
    }


    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CheLiang> selectRoleList(CheLiang role)
    {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRoleKeys(Long userId)
    {
        List<CheLiang> perms = roleMapper.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (CheLiang perm : perms)
        {
            if (StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    @Override
    public List<CheLiang> selectRolesByUserId(Long userId)
    {
        List<CheLiang> userRoles = roleMapper.selectRolesByUserId(userId);
        List<CheLiang> roles = selectRoleAll();
        for (CheLiang role : roles)
        {
            for (CheLiang userRole : userRoles)
            {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue())
                {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    @Override
    public List<CheLiang> selectRoleAll()
    {
        return SpringUtils.getAopProxy(this).selectRoleList(new CheLiang());
    }

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public CheLiang selectRoleById(Long roleId)
    {
        return roleMapper.selectRoleById(roleId);
    }

    @Override
    public boolean deleteRoleById(Long roleId) {
        // 删除车辆与控制关联
        roleDeptMapper.deleteRoleDeptByRoleId(roleId);
        return roleMapper.deleteRoleById(roleId) > 0 ? true : false;
    }

    @Override
    public int deleteRoleByIds(String ids) {
        Long[] roleIds = Convert.toLongArray(ids);
//        for (Long roleId : roleIds) {
//            Role role = selectRoleById(roleId);
//            if (countUserRoleByRoleId(roleId) > 0) {
//                throw new BusinessException(String.format("%1$s已分配,不能删除", role.getRoleName()));
//            }
//        }
        // 删除车辆与控制关联
        roleDeptMapper.deleteRoleDept(roleIds);
        return roleMapper.deleteRoleByIds(roleIds);
    }

    @Override
    public int insertRole(CheLiang role) {
        role.setCreateBy(ShiroUtils.getLoginName());
        // 新增角色信息
        roleMapper.insertRole(role);
        return insertRoleMenu(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(CheLiang role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<CheLiangKongZhi> list = new ArrayList<CheLiangKongZhi>();
        for (Long menuId : role.getMenuIds())
        {
            CheLiangKongZhi rm = new CheLiangKongZhi();
            rm.setRoleId(role.getRoleId());
            rm.setDeptId(menuId);
            list.add(rm);
        }
        if (list.size() > 0)
        {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }

    @Override
    public int updateRole(CheLiang role) {
        role.setUpdateBy(ShiroUtils.getLoginName());
        // 修改车辆信息
        roleMapper.updateRole(role);
        // 删除车辆与控制关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        return insertRoleMenu(role);
    }


    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int authDataScope(CheLiang role)
    {
        role.setUpdateBy(ShiroUtils.getLoginName());
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }



    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(CheLiang role)
    {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<CheLiangKongZhi> list = new ArrayList<CheLiangKongZhi>();
        for (Long deptId : role.getDeptIds())
        {
            CheLiangKongZhi rd = new CheLiangKongZhi();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0)
        {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }

    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(CheLiang role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        CheLiang info = roleMapper.checkRoleNameUnique(role.getRoleName());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.ROLE_NAME_NOT_UNIQUE;
        }
        return UserConstants.ROLE_NAME_UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(CheLiang role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        CheLiang info = roleMapper.checkRoleKeyUnique(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.ROLE_KEY_NOT_UNIQUE;
        }
        return UserConstants.ROLE_KEY_UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    @Override
    public void checkRoleAllowed(CheLiang role)
    {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin())
        {
            throw new BusinessException("不允许操作超级管理员角色");
        }
    }



    /**
     * 角色状态修改
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public int changeStatus(CheLiang role)
    {
        return roleMapper.updateRole(role);
    }






}
