package com.ruoyi.project.qiChe.QiCheBangDing.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 绑定对象 qi_che_bang_ding
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public class QiCheBangDing extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备id */
    private String id;

    /** 绑定名称 */
    @Excel(name = "绑定名称")
    private String mingCheng;

    /** 微信openid */
    @Excel(name = "微信openid")
    private String openid;

    /** 微信号 */
    @Excel(name = "微信号")
    private String weiXinHao;


    private String device_id;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setMingCheng(String mingCheng)
    {
        this.mingCheng = mingCheng;
    }

    public String getMingCheng()
    {
        return mingCheng;
    }
    public void setOpenid(String openid)
    {
        this.openid = openid;
    }

    public String getOpenid()
    {
        return openid;
    }
    public void setWeiXinHao(String weiXinHao)
    {
        this.weiXinHao = weiXinHao;
    }

    public String getWeiXinHao()
    {
        return weiXinHao;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mingCheng", getMingCheng())
            .append("openid", getOpenid())
            .append("weiXinHao", getWeiXinHao())
            .toString();
    }
}
