package com.ruoyi.project.qiChe.QiCheBangDing.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.qiChe.QiCheBangDing.domain.QiCheBangDing;
import com.ruoyi.project.qiChe.QiCheBangDing.service.IQiCheBangDingService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 绑定Controller
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@Controller
@RequestMapping("/qiChe/QiCheBangDing")
public class QiCheBangDingController extends BaseController
{
    private String prefix = "qiChe/QiCheBangDing";

    @Autowired
    private IQiCheBangDingService qiCheBangDingService;

    @RequiresPermissions("qiChe:QiCheBangDing:view")
    @GetMapping()
    public String QiCheBangDing()
    {
        return prefix + "/QiCheBangDing";
    }

    /**
     * 查询绑定列表
     */
    @RequiresPermissions("qiChe:QiCheBangDing:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(QiCheBangDing qiCheBangDing)
    {
        startPage();
        List<QiCheBangDing> list = qiCheBangDingService.selectQiCheBangDingList(qiCheBangDing);
        return getDataTable(list);
    }

    /**
     * 导出绑定列表
     */
    @RequiresPermissions("qiChe:QiCheBangDing:export")
    @Log(title = "绑定", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(QiCheBangDing qiCheBangDing)
    {
        List<QiCheBangDing> list = qiCheBangDingService.selectQiCheBangDingList(qiCheBangDing);
        ExcelUtil<QiCheBangDing> util = new ExcelUtil<QiCheBangDing>(QiCheBangDing.class);
        return util.exportExcel(list, "QiCheBangDing");
    }

    /**
     * 新增绑定
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存绑定
     */
    @RequiresPermissions("qiChe:QiCheBangDing:add")
    @Log(title = "绑定", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(QiCheBangDing qiCheBangDing)
    {
        return toAjax(qiCheBangDingService.insertQiCheBangDing(qiCheBangDing));
    }

    /**
     * 修改绑定
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        QiCheBangDing qiCheBangDing = qiCheBangDingService.selectQiCheBangDingById(id);
        mmap.put("qiCheBangDing", qiCheBangDing);
        return prefix + "/edit";
    }

    /**
     * 修改保存绑定
     */
    @RequiresPermissions("qiChe:QiCheBangDing:edit")
    @Log(title = "绑定", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(QiCheBangDing qiCheBangDing)
    {
        return toAjax(qiCheBangDingService.updateQiCheBangDing(qiCheBangDing));
    }

    /**
     * 删除绑定
     */
    @RequiresPermissions("qiChe:QiCheBangDing:remove")
    @Log(title = "绑定", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(qiCheBangDingService.deleteQiCheBangDingByIds(ids));
    }
}
