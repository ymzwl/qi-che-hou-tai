package com.ruoyi.project.qiChe.QiCheBangDing.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.qiChe.QiCheBangDing.mapper.QiCheBangDingMapper;
import com.ruoyi.project.qiChe.QiCheBangDing.domain.QiCheBangDing;
import com.ruoyi.project.qiChe.QiCheBangDing.service.IQiCheBangDingService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 绑定Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@Service
public class QiCheBangDingServiceImpl implements IQiCheBangDingService 
{
    @Autowired
    private QiCheBangDingMapper qiCheBangDingMapper;

    /**
     * 查询绑定
     * 
     * @param id 绑定ID
     * @return 绑定
     */
    @Override
    public QiCheBangDing selectQiCheBangDingById(String id)
    {
        return qiCheBangDingMapper.selectQiCheBangDingById(id);
    }

    /**
     * 查询绑定列表
     * 
     * @param qiCheBangDing 绑定
     * @return 绑定
     */
    @Override
    public List<QiCheBangDing> selectQiCheBangDingList(QiCheBangDing qiCheBangDing)
    {
        return qiCheBangDingMapper.selectQiCheBangDingList(qiCheBangDing);
    }

    /**
     * 新增绑定
     * 
     * @param qiCheBangDing 绑定
     * @return 结果
     */
    @Override
    public int insertQiCheBangDing(QiCheBangDing qiCheBangDing)
    {
        return qiCheBangDingMapper.insertQiCheBangDing(qiCheBangDing);
    }

    /**
     * 修改绑定
     * 
     * @param qiCheBangDing 绑定
     * @return 结果
     */
    @Override
    public int updateQiCheBangDing(QiCheBangDing qiCheBangDing)
    {
        return qiCheBangDingMapper.updateQiCheBangDing(qiCheBangDing);
    }

    /**
     * 删除绑定对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteQiCheBangDingByIds(String ids)
    {
        return qiCheBangDingMapper.deleteQiCheBangDingByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除绑定信息
     * 
     * @param id 绑定ID
     * @return 结果
     */
    @Override
    public int deleteQiCheBangDingById(String id)
    {
        return qiCheBangDingMapper.deleteQiCheBangDingById(id);
    }
}
