package com.ruoyi.project.qiChe.QiCheBangDing.mapper;

import java.util.List;
import com.ruoyi.project.qiChe.QiCheBangDing.domain.QiCheBangDing;

/**
 * 绑定Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public interface QiCheBangDingMapper 
{
    /**
     * 查询绑定
     * 
     * @param id 绑定ID
     * @return 绑定
     */
    public QiCheBangDing selectQiCheBangDingById(String id);

    /**
     * 查询绑定列表
     * 
     * @param qiCheBangDing 绑定
     * @return 绑定集合
     */
    public List<QiCheBangDing> selectQiCheBangDingList(QiCheBangDing qiCheBangDing);

    /**
     * 新增绑定
     * 
     * @param qiCheBangDing 绑定
     * @return 结果
     */
    public int insertQiCheBangDing(QiCheBangDing qiCheBangDing);

    /**
     * 修改绑定
     * 
     * @param qiCheBangDing 绑定
     * @return 结果
     */
    public int updateQiCheBangDing(QiCheBangDing qiCheBangDing);

    /**
     * 删除绑定
     * 
     * @param id 绑定ID
     * @return 结果
     */
    public int deleteQiCheBangDingById(String id);

    /**
     * 批量删除绑定
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteQiCheBangDingByIds(String[] ids);
}
