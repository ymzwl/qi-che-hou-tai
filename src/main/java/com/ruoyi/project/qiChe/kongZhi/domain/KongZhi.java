package com.ruoyi.project.qiChe.kongZhi.domain;

import javax.validation.constraints.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * 部门表 sys_dept
 * 
 * @author ruoyi
 */
public class KongZhi extends BaseEntity
{
    //,@RequestParam("tuBiaoFile") MultipartFile tuBiaoFileprivate static final long serialVersionUID = 1L;

    MultipartFile tuBiaoFile;//图标文件
    MultipartFile tuBiaoDianJiFile;//图标点击文件
    String tuBiaoLuJing;//图标文件服务器路径
    String tuBiaoDianJiLuJing;//图标点击文件服务器路径

    public String getTuBiaoLuJing() {
        return tuBiaoLuJing;
    }

    public void setTuBiaoLuJing(String tuBiaoLuJing) {
        this.tuBiaoLuJing = tuBiaoLuJing;
    }

    public String getTuBiaoDianJiLuJing() {
        return tuBiaoDianJiLuJing;
    }

    public void setTuBiaoDianJiLuJing(String tuBiaoDianJiLuJing) {
        this.tuBiaoDianJiLuJing = tuBiaoDianJiLuJing;
    }

    public MultipartFile getTuBiaoDianJiFile() {
        return tuBiaoDianJiFile;
    }

    public void setTuBiaoDianJiFile(MultipartFile tuBiaoDianJiFile) {
        this.tuBiaoDianJiFile = tuBiaoDianJiFile;
    }

    public MultipartFile getTuBiaoFile() {
        return tuBiaoFile;
    }

    public void setTuBiaoFile(MultipartFile tuBiaoFile) {
        this.tuBiaoFile = tuBiaoFile;
    }

    /** 部门ID */
    private Long deptId;

    /** 父部门ID */
    private Long parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 部门名称 */
    private String deptName;

    /** 显示顺序 */
    private String orderNum;

    /** 下发帧 */
    private String leader;
    /** 下发帧 */
    private String shang_xing_zhen;

    public String getShang_xing_zhen() {
        return shang_xing_zhen;
    }

    public void setShang_xing_zhen(String shang_xing_zhen) {
        this.shang_xing_zhen = shang_xing_zhen;
    }

    /** 联系电话 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 部门状态:0正常,1停用 */
    private String status;

    /** 图标是否是两态 0否 1是 */
    private String qi_che_liang_tai;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 父部门名称 */
    private String parentName;

    public Long getDeptId()
    {
        return deptId;
    }

    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getAncestors()
    {
        return ancestors;
    }

    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符")
    public String getDeptName()
    {
        return deptName;
    }

    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    @NotBlank(message = "显示顺序不能为空")
    public String getOrderNum()
    {
        return orderNum;
    }

    public void setOrderNum(String orderNum)
    {
        this.orderNum = orderNum;
    }

    public String getLeader()
    {
        return leader;
    }

    public void setLeader(String leader)
    {
        this.leader = leader;
    }

    @Size(min = 0, max = 11, message = "联系电话长度不能超过11个字符")
    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public String getQi_che_liang_tai() {
        return qi_che_liang_tai;
    }

    public void setQi_che_liang_tai(String qi_che_liang_tai) {
        this.qi_che_liang_tai = qi_che_liang_tai;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("deptId", getDeptId())
            .append("parentId", getParentId())
            .append("ancestors", getAncestors())
            .append("deptName", getDeptName())
            .append("orderNum", getOrderNum())
            .append("leader", getLeader())
                .append("tuBiaoLuJing", getTuBiaoLuJing())
                .append("tuBiaoDianJiLuJing", getTuBiaoDianJiLuJing())


            .append("phone", getPhone())
            .append("email", getEmail())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
