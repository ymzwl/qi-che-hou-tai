package com.ruoyi.project.qiChe.sheBei.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 设备对象 qi_che_she_bei
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public class QiCheSheBei extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备id */
    private String id;

    /** 绑定车辆 */
    @Excel(name = "绑定车辆")
    private String mingCheng;

    /** 模式 */
    @Excel(name = "模式")
    private Long qiCheCheLiangId;
    
    /**
     * 汽车车辆
     */
    private String moShiMingCheng;



    private String role_name;

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    /** 设备id(蓝牙id) */
    @Excel(name = "设备id(蓝牙id)")
    private String deviceId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setMingCheng(String mingCheng)
    {
        this.mingCheng = mingCheng;
    }

    public String getMingCheng()
    {
        return mingCheng;
    }
    public void setQiCheCheLiangId(Long qiCheCheLiangId)
    {
        this.qiCheCheLiangId = qiCheCheLiangId;
    }

    public Long getQiCheCheLiangId()
    {
        return qiCheCheLiangId;
    }
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mingCheng", getMingCheng())
            .append("qiCheCheLiangId", getQiCheCheLiangId())
            .append("deviceId", getDeviceId())
            .toString();
    }
}
