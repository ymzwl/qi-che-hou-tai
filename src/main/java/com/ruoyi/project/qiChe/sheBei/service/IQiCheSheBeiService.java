package com.ruoyi.project.qiChe.sheBei.service;

import java.util.List;
import com.ruoyi.project.qiChe.sheBei.domain.QiCheSheBei;

/**
 * 设备Service接口
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public interface IQiCheSheBeiService 
{
    /**
     * 查询设备
     * 
     * @param id 设备ID
     * @return 设备
     */
    public QiCheSheBei selectQiCheSheBeiById(String id);

    /**
     * 查询设备列表
     * 
     * @param qiCheSheBei 设备
     * @return 设备集合
     */
    public List<QiCheSheBei> selectQiCheSheBeiList(QiCheSheBei qiCheSheBei);

    /**
     * 新增设备
     * 
     * @param qiCheSheBei 设备
     * @return 结果
     */
    public int insertQiCheSheBei(QiCheSheBei qiCheSheBei);

    /**
     * 修改设备
     * 
     * @param qiCheSheBei 设备
     * @return 结果
     */
    public int updateQiCheSheBei(QiCheSheBei qiCheSheBei);

    /**
     * 批量删除设备
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteQiCheSheBeiByIds(String ids);

    /**
     * 删除设备信息
     * 
     * @param id 设备ID
     * @return 结果
     */
    public int deleteQiCheSheBeiById(String id);
}
