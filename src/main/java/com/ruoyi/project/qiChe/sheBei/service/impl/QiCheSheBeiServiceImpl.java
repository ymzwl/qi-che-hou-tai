package com.ruoyi.project.qiChe.sheBei.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.qiChe.sheBei.mapper.QiCheSheBeiMapper;
import com.ruoyi.project.qiChe.sheBei.domain.QiCheSheBei;
import com.ruoyi.project.qiChe.sheBei.service.IQiCheSheBeiService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 设备Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@Service
public class QiCheSheBeiServiceImpl implements IQiCheSheBeiService 
{
    @Autowired
    private QiCheSheBeiMapper qiCheSheBeiMapper;

    /**
     * 查询设备
     * 
     * @param id 设备ID
     * @return 设备
     */
    @Override
    public QiCheSheBei selectQiCheSheBeiById(String id)
    {
        return qiCheSheBeiMapper.selectQiCheSheBeiById(id);
    }

    /**
     * 查询设备列表
     * 
     * @param qiCheSheBei 设备
     * @return 设备
     */
    @Override
    public List<QiCheSheBei> selectQiCheSheBeiList(QiCheSheBei qiCheSheBei)
    {
        return qiCheSheBeiMapper.selectQiCheSheBeiList(qiCheSheBei);
    }

    /**
     * 新增设备
     * 
     * @param qiCheSheBei 设备
     * @return 结果
     */
    @Override
    public int insertQiCheSheBei(QiCheSheBei qiCheSheBei)
    {
        return qiCheSheBeiMapper.insertQiCheSheBei(qiCheSheBei);
    }

    /**
     * 修改设备
     * 
     * @param qiCheSheBei 设备
     * @return 结果
     */
    @Override
    public int updateQiCheSheBei(QiCheSheBei qiCheSheBei)
    {
        return qiCheSheBeiMapper.updateQiCheSheBei(qiCheSheBei);
    }

    /**
     * 删除设备对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteQiCheSheBeiByIds(String ids)
    {
        return qiCheSheBeiMapper.deleteQiCheSheBeiByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除设备信息
     * 
     * @param id 设备ID
     * @return 结果
     */
    @Override
    public int deleteQiCheSheBeiById(String id)
    {
        return qiCheSheBeiMapper.deleteQiCheSheBeiById(id);
    }
}
