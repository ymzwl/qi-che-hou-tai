package com.ruoyi.project.qiChe.sheBei.controller;

import java.util.List;
import java.util.Random;

import com.ruoyi.project.qiChe.cheLiang.service.ICheLiangService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.qiChe.sheBei.domain.QiCheSheBei;
import com.ruoyi.project.qiChe.sheBei.service.IQiCheSheBeiService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 设备Controller
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@Controller
@RequestMapping("/qiChe/sheBei")
public class QiCheSheBeiController extends BaseController
{
    private String prefix = "qiChe/sheBei";

    @Autowired
    private ICheLiangService roleService;

    @Autowired
    private IQiCheSheBeiService qiCheSheBeiService;

    @RequiresPermissions("qiChe:sheBei:view")
    @GetMapping()
    public String sheBei()
    {
        return prefix + "/sheBei";
    }

    /**
     * 查询设备列表
     */
    @RequiresPermissions("qiChe:sheBei:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(QiCheSheBei qiCheSheBei)
    {
        startPage();
        List<QiCheSheBei> list = qiCheSheBeiService.selectQiCheSheBeiList(qiCheSheBei);
        return getDataTable(list);
    }

    /**
     * 导出设备列表
     */
    @RequiresPermissions("qiChe:sheBei:export")
    @Log(title = "设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(QiCheSheBei qiCheSheBei)
    {
        List<QiCheSheBei> list = qiCheSheBeiService.selectQiCheSheBeiList(qiCheSheBei);
        ExcelUtil<QiCheSheBei> util = new ExcelUtil<QiCheSheBei>(QiCheSheBei.class);
        return util.exportExcel(list, "sheBei");
    }

    /**
     * 新增设备
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("posts", roleService.selectRoleAll());
        return prefix + "/add";
    }


    /**
     * 新增保存设备
     */
    @RequiresPermissions("qiChe:sheBei:add")
    @Log(title = "设备", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(QiCheSheBei qiCheSheBei)
    {
    	qiCheSheBei.setId(IdUtils.randomUUID());
        return toAjax(qiCheSheBeiService.insertQiCheSheBei(qiCheSheBei));
    }

    /**
     * 修改设备
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        QiCheSheBei qiCheSheBei = qiCheSheBeiService.selectQiCheSheBeiById(id);
        mmap.put("qiCheSheBei", qiCheSheBei);
        mmap.put("posts", roleService.selectRoleAll());
        return prefix + "/edit";
    }

    /**
     * 修改保存设备
     */
    @RequiresPermissions("qiChe:sheBei:edit")
    @Log(title = "设备", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(QiCheSheBei qiCheSheBei)
    {
        return toAjax(qiCheSheBeiService.updateQiCheSheBei(qiCheSheBei));
    }

    /**
     * 删除设备
     */
    @RequiresPermissions("qiChe:sheBei:remove")
    @Log(title = "设备", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(qiCheSheBeiService.deleteQiCheSheBeiByIds(ids));
    }
}
